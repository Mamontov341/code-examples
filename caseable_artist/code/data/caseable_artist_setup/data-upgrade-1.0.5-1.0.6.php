<?php

/**
 * Caseable artist data upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com). All rights served.
 * @version   1.0.3
 */
/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();


//clean old install if necessary
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode('artist', 'commision_currency');

if ($attrId){
    $entityTypeId = $installer->getEntityTypeId('artist');
    $tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';
    
    $installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE `attribute_id` = '$attrId';");
    
    $installer->removeAttribute(Caseable_Artist_Model_Artist::ENTITY, 'commision_currency');
}


//add new attribute
$defaultValue = Mage::app()->getBaseCurrencyCode();
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'commission_currency', array(
    'type' => 'varchar',
    'input' => 'select',
    'source' => 'artist/system_config_source_currency',
    'backend' => 'eav/entity_attribute_backend_array',
    'label' => 'Commission currency',
    'required' => true,
    'sort_order' => 310,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'group' => 'Billing Information',
    'default' => $defaultValue,
));

//Update all artists with default currency
$entityTypeId = $installer->getEntityTypeId('artist');
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode('artist', 'commision_currency');
$baseCurrency = Mage::app()->getBaseCurrencyCode();


$tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';
$sql = "SELECT entity_id FROM `{$this->getTable('artist/artist')}` GROUP BY entity_id";
try {
    $result = $installer->getConnection()->fetchAll($sql);
    $casheIds = array();
    foreach ($result as $artist) {
        $entityId = $artist['entity_id'];

        $update = "INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                . " VALUES (NULL," . $entityTypeId . ",0," . $attrId . "," . $entityId . ",'" . $baseCurrency . "')";

        $installer->run($update);
    }
} catch (Exception $e) {
        Mage::logException($e);
        
        $artistList = Mage::getModel('artist/artist')->getCollection();
        foreach ($artistList as $artist) {
            try {
                $artist->setData('commission_currency', $defaultValue);
                $artist->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
}


$installer->endSetup();
