<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';

$defaultPercentValue = 6;
$attributeCode = 'commision_percent';
$entity = Caseable_Artist_Model_Artist::ENTITY;
$entityTypeId = $installer->getEntityTypeId($entity);

//clean old install if necessary
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);

/*
$artistList = Mage::getModel('artist/artist')->getCollection();
foreach ($artistList as $artist) {
    try {
        $artist->setData($attributeCode, $defaultPercentValue);
        $artist->save();
    } catch (Exception $e) {        
        Mage::logException($e);
    }
}
 * 
 */
//fix issue with sql from 1.0.7 
$installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE (`attribute_id`=0); ");

if($attrId){
    
    $installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE (`attribute_id`=$attrId); ");

    //Update all artists with percent commision
    $sql = "SELECT entity_id FROM `{$this->getTable('artist/artist')}` GROUP BY entity_id";

    $result = $installer->getConnection()->fetchAll($sql);
    $sql = '';

    
    foreach ($result as $artist) {
        $entityId = $artist['entity_id'];

        $sql.= "INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                . " VALUES (NULL," . $entityTypeId . ",0," . $attrId . "," . $entityId . ",'" . $defaultPercentValue . "');";

    }

    $installer->run($sql);
}
$installer->endSetup();
