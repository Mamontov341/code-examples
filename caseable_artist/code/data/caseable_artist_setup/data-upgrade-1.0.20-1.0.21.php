<?php
/**
 * Caseable artist data upgrade file
 *
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 23.12.2016
 * Time: 11:43 */


/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();


//clean old install if necessary
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode('artist', 'frontend_position');

if ($attrId){
    $entityTypeId = $installer->getEntityTypeId('artist');
    $tblArtistAttrInt = $this->getTable('artist/artist') . '_int';

    $installer->run("DELETE FROM `$tblArtistAttrInt` WHERE `attribute_id` = '$attrId';");
}

$defaultValue = 1;

//Update all artists frontend positions



$sql = "SELECT entity_id FROM `{$this->getTable('artist/artist')}` GROUP BY entity_id";
try {
    $result = $installer->getConnection()->fetchAll($sql);
    $casheIds = array();
    $i = 1;
    foreach ($result as $artist) {
        $entityId = $artist['entity_id'];

        $update = "INSERT INTO `$tblArtistAttrInt` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
            . " VALUES (NULL," . $entityTypeId . ",0," . $attrId . "," . $entityId . ",'" . $i . "')";

        $installer->run($update);
        $i++;
    }
} catch (Exception $e) {
    Mage::logException($e);

    $artistList = Mage::getModel('artist/artist')->getCollection();
    foreach ($artistList as $artist) {
        try {
            $artist->setData('frontend_position', $defaultValue);
            $artist->save();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}


$installer->endSetup();
