<?php

/**
 * Caseable artist data upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com). All rights served.
 * @version   1.0.3
 */

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'commision_percent', array(
    'type' => 'varchar',
    'label' => 'Commision percent',
    'input' => 'text',
    'required' => true,
    'sort_order' => 300,
    'global' =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'Billing Information',
));
$installer->endSetup();