<?php

/**
 * Caseable artist data upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com). All rights served.
 * @version   1.0.3
 */

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->update(
    $installer->getTable('artist/artist'),
    array('artist_active' => 2),
    array('artist_active = ?' => 0)
);

$installer->endSetup();