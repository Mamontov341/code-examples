<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

//Artist,website,app,marketplace
$data = array(
    "Ali Gulec" => array(10, 6, 6),
    "Aurelie Scour" => array(10, 6, 6),
    "Berlin Calling" => array(10, 6, 6),
    "Bianca Green" => array(10, 6, 6),
    "BioWorkZ" => array(10, 6, 6),
    "Bitsofbobs" => array(10, 6, 6),
    "Brent Williams" => array(10, 6, 6),
    "Claus Peter Schöps" => array(10, 6, 6),
    "Daniel Martin Diaz" => array(10, 10, 10),
    "Danny Ivan" => array(10, 6, 6),
    "Fists et Lettres" => array(18, 6, 6),
    "Enkel Dika" => array(10, 6, 6),
    "Eric Fan" => array(10, 6, 6),
    "Gela Behrmann" => array(10, 6, 6),
    "IRMA" => array(20, 6, 6),
    "IZAK" => array(10, 10, 10),
    "Jamison Ernest" => array(50, 6, 6),
    "Joy StClaire" => array(10, 6, 6),
    "Julien Durix" => array(10, 6, 6),
    "Kaitlyn Parker" => array(10, 6, 6),
    "Kreatyves" => array(10, 6, 6),
    "Khristian Howell" => array(10, 10, 10),
    "Leah Flores" => array(10, 6, 6),
    "Lovoo" => array(18, 6, 6),
    "Lyric Culture" => array(15, 15, 15),
    "Mareike Böhmer" => array(10, 6, 6),
    "Marie Luise Schmidt" => array(10, 6, 6),
    "Mark Ashkenazi" => array(10, 6, 6),
    "Mat Miller" => array(10, 6, 6),
    "Mattartiste" => array(10, 6, 6),
    "Minjae Lee" => array(10, 6, 6),
    "Ronya Galka" => array(10, 6, 6),
    "Sandrine Pagnoux" => array(10, 6, 6),
    "Shell Rummel" => array(10, 10, 10),
    "Simon+Kabuki" => array(10, 10, 10),
    "Skelanimals" => array(10, 10, 10),
    "SO SO Happy" => array(10, 10, 10),
    "Spires" => array(10, 6, 6),
    "Terri Henson" => array(10, 10, 10),
    "Terry Fan" => array(10, 6, 6),
    "The Shit Shop" => array(10, 6, 6),
    "Tom Baecker" => array(10, 6, 6),
    "Tom Christopher" => array(10, 6, 6),
    "VISUAL STATEMENTS" => array(10, 6, 6),
    "Uglydoll" => array(10, 10, 10),
    "WE MAKE THE CAKE" => array(10, 6, 6)
);

$entity = Caseable_Artist_Model_Artist::ENTITY;
$entityTypeId = $installer->getEntityTypeId($entity);
$tblCommision = $this->getTable('artist/commision');
$sql = "TRUNCATE TABLE $tblCommision";


/*
 * 1. truncate table
 * 2. split data by artist code and type of channel
 * 3. insert data by each channel type and artistID
 */

$installer->run($sql);

$channelCollection = Mage::getModel('caseable_core/channel')->getCollection();
$channelCollection->getSelect()->order('type', 'DESC');
$channelCollection->load();
$channels = array();
foreach ($channelCollection as $item) {
    $channels[] = $item->getData();
}

$artistList = Mage::getModel('artist/artist')->getCollection()
        ->addAttributeToSelect('profile_name')
        ->addAttributeToFilter('profile_name', array('neq' => ''));

$isNotFound = array();

foreach ($artistList as $artist) {
    $frontendId = null;
    $artistId = null;
    $commision = null;
    $insertSql = '';

    $artistId = $artist->getData('entity_id');
    $profileName = $artist->getData('profile_name');
    $profileName = trim($profileName);

    if (isset($data[$profileName])) {
        $artistCommisions = $data[$profileName];
    } else {
        $isNotFound[] = $profileName;
        $artistCommisions = array(
            Mage::getResourceModel('artist/commision')->getDefaultCommisionByChannelType(Caseable_Core_Model_Channel::TYPE_WEBSITE),
            Mage::getResourceModel('artist/commision')->getDefaultCommisionByChannelType(Caseable_Core_Model_Channel::TYPE_APP),
            Mage::getResourceModel('artist/commision')->getDefaultCommisionByChannelType(Caseable_Core_Model_Channel::TYPE_MARKETPLACE),
        );
    }

    foreach ($channels as $item) {
        $frontendId = $item['frontend_id'];
        $type = $item['type'];

        if ($type == Caseable_Core_Model_Channel::TYPE_WEBSITE) {
            $commision = $artistCommisions[0];
            try {
                $insertSql = "INSERT INTO `$tblCommision` (`entity_id`, `frontend_id`, `artist_entity_id`, `commision`)"
                        . " VALUES (NULL," . $frontendId . "," . $artistId . "," . $commision . ");";
                $installer->run($insertSql);
            } catch (Exception $e) {
                $model = Mage::getModel('artist/commision')
                        ->setFrontendId($frontendId)
                        ->setArtistEntityId($artistId)
                        ->setCommision($commision)
                        ->save();
            }
        } elseif ($type == Caseable_Core_Model_Channel::TYPE_APP) {
            $commision = $artistCommisions[1];
            try {
                $insertSql = "INSERT INTO `$tblCommision` (`entity_id`, `frontend_id`, `artist_entity_id`, `commision`)"
                        . " VALUES (NULL," . $frontendId . "," . $artistId . "," . $commision . ");";
                $installer->run($insertSql);
            } catch (Exception $e) {
                $model = Mage::getModel('artist/commision')
                        ->setFrontendId($frontendId)
                        ->setArtistEntityId($artistId)
                        ->setCommision($commision)
                        ->save();
            }
        } elseif ($type == Caseable_Core_Model_Channel::TYPE_MARKETPLACE) {
            $commision = $artistCommisions[2];
            try {
                $insertSql = "INSERT INTO `$tblCommision` (`entity_id`, `frontend_id`, `artist_entity_id`, `commision`)"
                        . " VALUES (NULL," . $frontendId . "," . $artistId . "," . $commision . ");";
                $installer->run($insertSql);
            } catch (Exception $e) {
                $model = Mage::getModel('artist/commision')
                        ->setFrontendId($frontendId)
                        ->setArtistEntityId($artistId)
                        ->setCommision($commision)
                        ->save();
            }
        }
    }   
}

//Mage::log($isNotFound);

$installer->endSetup();

