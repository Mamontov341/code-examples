<?php
/**
 * Caseable artist data upgrade file
 *
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 23.12.2016
 * Time: 11:43 */

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'artist_active', array(
    'type' => 'static',
    'label' => 'Active',
    'input' => 'select',
    'required' => true,
    'source', 'caseable_core/entity_attribute_source_boolean',
    'sort_order' => 1000,
    'global' => 1,
    'group' => 'Artist Profile'
));

$installer->endSetup();