<?php

/**
 * Caseable artist data upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com). All rights served.
 * @version   1.0.2
 */

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$installer->removeAttribute(Caseable_Artist_Model_Artist::ENTITY, 'image');
$installer->removeAttribute(Caseable_Artist_Model_Artist::ENTITY, 'banner');

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_image', array(
    'type' => 'varchar',
    'label' => 'Profile Image',
    'input' => 'dropzone_image',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 60,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_banner', array(
    'type' => 'varchar',
    'label' => 'Profile Banner',
    'input' => 'dropzone_banner',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 70,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));

$installer->endSetup();