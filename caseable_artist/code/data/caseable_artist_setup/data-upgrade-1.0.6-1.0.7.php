<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';

$installer->run(" 
ALTER TABLE `{$tblArtistAttrVarchar}`
    ADD CONSTRAINT `FK_EAV_ATTRIBUTE_ATTRIBUTE_ID_CASEABLE_DRAFT_ENTITY_VARCHAR` FOREIGN KEY (`attribute_id`) REFERENCES `{$installer->getTable('eav/attribute')}` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE;
   
");

$defaultPercentValue = 6;
$attributeCode = 'commision_percent';
$entity = Caseable_Artist_Model_Artist::ENTITY;
//clean old install if necessary
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);

if ($attrId){
    $entityTypeId = $installer->getEntityTypeId($entity);
    $tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';
    
    $installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE `attribute_id` = '$attrId';");
    
    $installer->removeAttribute($entity, $attributeCode);
}

//add new attribute
$defaultValue = Mage::app()->getBaseCurrencyCode();
$installer->addAttribute($entity, $attributeCode, array(
    'type' => 'decimal',
    'label' => 'Commision percent',
    'input' => 'text',
    'required' => true,
    'sort_order' => 300,
    'global' =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'Billing Information',
));

/*
$artistList = Mage::getModel('artist/artist')->getCollection();
foreach ($artistList as $artist) {
    try {
        $artist->setData($attributeCode, $defaultPercentValue);
        $artist->save();
    } catch (Exception $e) {        
        Mage::logException($e);
    }
}
 * 
 */

//Update all artists with percent commision
$entityTypeId = $installer->getEntityTypeId('artist');
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode('artist', $attributeCode);
$sql = "SELECT entity_id FROM `{$this->getTable('artist/artist')}` GROUP BY entity_id";

$result = $installer->getConnection()->fetchAll($sql);
$sql = '';
if($attrId){
    foreach ($result as $artist) {
        $entityId = $artist['entity_id'];

        $sql.= "INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                . " VALUES (NULL," . $entityTypeId . ",0," . $attrId . "," . $entityId . ",'" . $defaultPercentValue . "');";

    }
    Mage::log($sql);
    $installer->run($sql);
}
$installer->endSetup();
