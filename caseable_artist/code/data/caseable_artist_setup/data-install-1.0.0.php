<?php

/**
 * Caseable artist setup file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */

/* @var $installer Caseable_Artist_Model_Setup */
$installer = $this;

/**
 * Convert xml data to array
 *
 * @param $xml
 * @return array
 */
function xml2array($xml)
{
    $arr = array();

    foreach ($xml as $element) {
        $tag = $element->getName();
        $e = get_object_vars($element);

        if (!empty($e)) {
            $arr[$tag] = $element instanceof SimpleXMLElement ? xml2array($element) : $e;
        } else {
            $arr[$tag] = trim($element);
        }
    }

    return $arr;
}

$feed = $homepage = file_get_contents('http://de.caseable.com/artistfeed.xml');
$feedObject = simplexml_load_string($feed);

foreach ($feedObject as $artist) {
    $artist = xml2array($artist);
    $model = Mage::getModel('artist/artist');

    // Address information
    $model->setFirstname($artist['billingInformation']['billFirstName']);
    $model->setLastname($artist['billingInformation']['billLastName']);
    $model->setStreet($artist['billingInformation']['billAddress']);
    $model->setCity($artist['billingInformation']['billTownCity']);
    $model->setPostcode($artist['billingInformation']['billZipPostCode']);
    $model->setTelephone($artist['billingInformation']['phoneNo']);

    // Payment data
    $model->setPaypalAccount($artist['email']);

    // Profile data
    $model->setProfileName($artist['langcode-1']['artistName']);
    $model->setMetaTitle($artist['langcode-1']['artistTitle']);
    $model->setMetaDescription($artist['langcode-1']['artistDescription']);
    $model->setArtistActive(1);
    $model->save();
}