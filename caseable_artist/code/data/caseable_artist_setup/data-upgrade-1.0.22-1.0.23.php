<?php
/**
 * Caseable artist data upgrade file
 *
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 23.12.2016
 * Time: 11:43 */

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'sku_pattern', array(
    'type' => 'static',
    'label' => 'SKU Pattern',
    'input' => 'text',
    'required' => true,
    'sort_order' => 10,
    'global' => 1,
    'group' => 'Artist Profile'
));

$installer->endSetup();