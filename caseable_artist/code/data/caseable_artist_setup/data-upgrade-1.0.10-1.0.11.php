<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();



$entity                 = Caseable_Artist_Model_Artist::ENTITY;
$entityTypeId           = $installer->getEntityTypeId($entity);
$tblArtistAttrVarchar   = $this->getTable('artist/artist') . '_varchar';
$tblEav                 = $this->getTable('eav/attribute');
$sql = "DELETE FROM $tblArtistAttrVarchar WHERE attribute_id NOT IN (SELECT attribute_id FROM $tblEav WHERE entity_type_id=$entityTypeId)";

$installer->run($sql);

$defaultPercentValue = 6;
$defaultCurrencyCode = Mage::app()->getBaseCurrencyCode();

$attributeCode  = 'commision_percent';
$attributeCode2 = 'commision_currency';

$attrPercentId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);
$attrCurrencyId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode2);

if($attrCurrencyId && $attrPercentId){

    //Insert default currency
    $sql = "SELECT entity_id FROM `{$this->getTable('artist/artist')}` GROUP BY entity_id";
    try {
        $result = $installer->getConnection()->fetchAll($sql);
        $casheIds = array();
        $insert = '';
        foreach ($result as $artist) {
            $entityId = $artist['entity_id'];

            $insert.= "INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                    . " VALUES (NULL," . $entityTypeId . ",0," . $attrCurrencyId . "," . $entityId . ",'" . $defaultCurrencyCode . "');";
            $insert.= "INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                    . " VALUES (NULL," . $entityTypeId . ",0," . $attrPercentId . "," . $entityId . ",'" . $defaultPercentValue . "');";
            
        }
        $installer->run($insert);
    } catch (Exception $e) {
            Mage::logException($e);

            $artistList = Mage::getModel('artist/artist')->getCollection();
            foreach ($artistList as $artist) {
                try {
                    $artist->setData($attributeCode, $defaultPercentValue);
                    $artist->setData($attributeCode2, $defaultCurrencyCode);
                    $artist->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
    }
}

if($attrCurrencyId && $attrPercentId){
    //Update artist with specific commision persent value
    $currencyCode = 'USD';
    $data = array(
        'IZAK' =>10,
        'Lyric Culture'=>15,
        'Shell Rummel'=>10,
        'Simon + Kabuki'=>10,
        'Skelanimals'=>10,
        'SO SO Happy'=>10,
        'Terri Henson'=>10,
        'Uglydoll'=>10,    
    );

    $names = array_keys($data);

    $artistList = Mage::getModel('artist/artist')->getCollection()
        ->addAttributeToSelect('profile_name')
        ->addAttributeToFilter('profile_name', array('in' => array('profile_name' => $names)));
    $sql = '';
    foreach ($artistList as $artist) {
        $profileName    = $artist->getData('profile_name');
        $entId          = $artist->getData('entity_id');
        $profileName    = trim($profileName);

        if(isset($data[$profileName])){
            $commisionValue = $data[$profileName];

            $sql.= "UPDATE `{$tblArtistAttrVarchar}` SET `value`=" . $commisionValue . " WHERE (`attribute_id`=" . $attrPercentId . " AND `entity_id`=" . $entId . ");";
            $sql.= "UPDATE `{$tblArtistAttrVarchar}` SET `value`='" . $currencyCode . "' WHERE (`attribute_id`=" . $attrCurrencyId . " AND `entity_id`=" . $entId . ");";
        }    
    }

    $installer->run($sql);
}
$installer->endSetup();
