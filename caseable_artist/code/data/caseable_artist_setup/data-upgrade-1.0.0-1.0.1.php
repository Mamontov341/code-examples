<?php

/**
 * Caseable artist data upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   1.0.1
 */

/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

/* @var $category Caseable_Artist_Model_Artist */
$artist = Mage::getModel('artist/artist');

$entityTypeId = $installer->getEntityTypeId(Caseable_Artist_Model_Artist::ENTITY);
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);

$installer->removeAttributeGroup($entityTypeId, $attributeSetId, 'General Information');
$installer->removeAttributeGroup($entityTypeId, $attributeSetId, 'Profile');

$installer->addAttributeGroup($entityTypeId, $attributeSetId, 'General Information', 0);

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'sku_pattern', array(
    'type' => 'static',
    'label' => 'SKU Pattern',
    'input' => 'text',
    'required' => true,
    'sort_order' => 10,
    'global' => SCOPE_GLOBAL,
    'group' => 'General Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'artist_active', array(
    'type' => 'static',
    'label' => 'Active',
    'input' => 'select',
    'sort_order' => 20,
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => SCOPE_GLOBAL,
    'group' => 'General Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_name', array(
    'type' => 'varchar',
    'label' => 'Profile Name',
    'input' => 'text',
    'required' => true,
    'sort_order' => 30,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
    'unique' => true,
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_text', array(
    'type' => 'text',
    'label' => 'Profile Text',
    'input' => 'textarea',
    'required' => true,
    'sort_order' => 40,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
    'wysiwyg' => true,
    'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_country', array(
    'type' => 'varchar',
    'label' => 'Country',
    'input' => 'select',
    'source' => 'customer/entity_address_attribute_source_country',
    'required' => false,
    'sort_order' => 50,
    'global' => SCOPE_GLOBAL,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_image', array(
    'type' => 'varchar',
    'label' => 'Profile Image',
    'input' => 'dropzone_image',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 60,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_banner', array(
    'type' => 'varchar',
    'label' => 'Profile Banner',
    'input' => 'dropzone_banner',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 70,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'meta_title', array(
    'type' => 'varchar',
    'label' => 'Page Title',
    'class' => 'validate-length maximum-length-67',
    'validate_rules' => 'a:2:{s:15:"max_text_length";i:67;s:15:"min_text_length";i:1;}',
    'note' => 'Maximum 67 chars',
    'input' => 'text',
    'required' => false,
    'sort_order' => 80,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'meta_description', array(
    'type' => 'text',
    'label' => 'Page Description',
    'class' => 'validate-length maximum-length-155',
    'validate_rules' => 'a:2:{s:15:"max_text_length";i:155;s:15:"min_text_length";i:1;}',
    'note' => 'Maximum 155 chars',
    'input' => 'textarea',
    'required' => false,
    'sort_order' => 90,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'paypal_account', array(
    'type' => 'varchar',
    'label' => 'Paypal Account',
    'input' => 'text',
    'required' => true,
    'sort_order' => 95,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'prefix', array(
    'type' => 'varchar',
    'label' => 'Prefix',
    'input' => 'text',
    'required' => false,
    'sort_order' => 100,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'first_name', array(
    'type' => 'varchar',
    'label' => 'First Name',
    'input' => 'text',
    'required' => true,
    'sort_order' => 110,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'middle_name', array(
    'type' => 'varchar',
    'label' => 'Middle Name/Initial',
    'input' => 'text',
    'required' => false,
    'sort_order' => 120,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'last_name', array(
    'type' => 'varchar',
    'label' => 'Last Name',
    'input' => 'text',
    'required' => true,
    'sort_order' => 130,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'suffix', array(
    'type' => 'varchar',
    'label' => 'Suffix',
    'input' => 'text',
    'required' => false,
    'sort_order' => 140,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'company', array(
    'type' => 'varchar',
    'label' => 'Company',
    'input' => 'text',
    'required' => false,
    'sort_order' => 150,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'street', array(
    'type' => 'varchar',
    'label' => 'Street Address',
    'input' => 'text',
    'required' => false,
    'sort_order' => 160,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'city', array(
    'type' => 'varchar',
    'label' => 'City',
    'input' => 'text',
    'required' => false,
    'sort_order' => 170,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
    'unique' => true,
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'country', array(
    'type' => 'varchar',
    'label' => 'Country',
    'input' => 'select',
    'source' => 'customer/entity_address_attribute_source_country',
    'required' => false,
    'sort_order' => 180,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'region', array(
    'type' => 'varchar',
    'label' => 'State/Province',
    'input' => 'text', /** @todo make to dropdown */
    'required' => false,
    'sort_order' => 190,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'postal_code', array(
    'type' => 'varchar',
    'label' => 'Zip/Postal Code',
    'input' => 'text',
    'required' => false,
    'sort_order' => 200,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'telephone', array(
    'type' => 'varchar',
    'label' => 'Telephone',
    'input' => 'text',
    'required' => false,
    'sort_order' => 210,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'fax', array(
    'type' => 'varchar',
    'label' => 'Fax',
    'input' => 'text',
    'required' => false,
    'sort_order' => 220,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));
$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'vat_number', array(
    'type' => 'varchar',
    'label' => 'VAT number',
    'input' => 'text',
    'required' => false,
    'sort_order' => 230,
    'global' => SCOPE_GLOBAL,
    'group' => 'Billing Information',
));

$installer->endSetup();