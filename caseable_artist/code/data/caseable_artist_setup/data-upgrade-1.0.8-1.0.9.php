<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$entity = Caseable_Artist_Model_Artist::ENTITY;
$entityTypeId = $installer->getEntityTypeId($entity);

$tblArtistAttrVarchar = $this->getTable('artist/artist') . '_varchar';
$currencyCode = 'USD';
$attributeCode = 'commision_percent';
$attributeCode2 = 'commision_currency';
$attrPercentId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);
$attrCurrencyId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode2);


//DEFAULT commision percent 
if($attrPercentId) {
    $defaultPercentValue = 6;
    $artistList = Mage::getModel('artist/artist')->getCollection();
    foreach($artistList as $artist){
        $artistId = $artist->getData('entity_id');

        $installer->run("DELETE FROM $tblArtistAttrVarchar WHERE `attribute_id` = $attrPercentId AND `entity_id` = $artistId;");
        $installer->run("INSERT INTO `$tblArtistAttrVarchar` (`value_id`, `entity_type_id`, `store_id`, `attribute_id`, `entity_id`, `value`)"
                    . " VALUES (NULL," . $entityTypeId . ",0," . $attrPercentId . "," . $artistId . ",'" . $defaultPercentValue . "');");    
    }
}

//Update artist with specific commision persent value
$data = array(
    'IZAK' =>10,
    'Lyric Culture'=>15,
    'Shell Rummel'=>10,
    'Simon + Kabuki'=>10,
    'Skelanimals'=>10,
    'SO SO Happy'=>10,
    'Terri Henson'=>10,
    'Uglydoll'=>10,    
);

$names = array_keys($data);

$artistList = Mage::getModel('artist/artist')->getCollection()
    ->addAttributeToSelect('profile_name')
    ->addAttributeToFilter('profile_name', array('in' => array('profile_name' => $names)));
$sql = '';
foreach ($artistList as $artist) {
    $profileName = $artist->getData('profile_name');
    $entId = $artist->getData('entity_id');
    $profileName = trim($profileName);
    
    if(isset($data[$profileName])){
        $commisionValue = $data[$profileName];
        
        $sql.= "UPDATE `{$tblArtistAttrVarchar}` SET `value`=" . $commisionValue . " WHERE (`attribute_id`=" . $attrPercentId . " AND `entity_id`=" . $entId . ");";
        $sql.= "UPDATE `{$tblArtistAttrVarchar}` SET `value`='" . $currencyCode . "' WHERE (`attribute_id`=" . $attrCurrencyId . " AND `entity_id`=" . $entId . ");";
    }    
}

$installer->run($sql);

//Delete mock attribute
$attributeCode = "commission_currency";
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);
if ($attrId){
    $installer->removeAttribute($entity, $attributeCode);
}


$installer->endSetup();
