<?php

class Caseable_Artist_Model_Commision extends Mage_Core_Model_Abstract {

    const CACHE_TAG = 'artist_commision';

    protected $_eventPrefix = 'artist';
    protected $_eventObject = 'commision';

    protected function _construct() {
        $this->_init('artist/commision');
    }

    protected function _getResource() {
        return Mage::getResourceSingleton('artist/commision');
    }

    public function saveCommisions($artist) {
        $data = $artist->getCommisionsData();
        if (!is_null($data)) {
            $this->_getResource()->saveCommisions($artist, $data);
        }
        return $this;
    }

    public function getCommisionByArtistChannel($artistEntId, $frontendId) {
        return $this->_getResource()->getCommisionByArtistChannel($artistEntId, $frontendId);
    }

    public function getDefaultCommisionByChannelType($type) {
        return $this->_getResource()->getDefaultCommisionByChannelType($type);
    }

    public function getDefaultCommisionValue(){
        return $this->_getResource()->getDefaultCommisionValue();
    }
}
