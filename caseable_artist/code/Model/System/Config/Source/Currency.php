<?php

class Caseable_Artist_Model_System_Config_Source_Currency extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions() {
        $options = array();
        $currencies =  Mage::getModel('directory/currency')->getConfigAllowCurrencies(); //Mage::getModel("adminhtml/system_config_source_currency")->getConfigAllowCurrencies();

        foreach($currencies as $code){
            if($code){
                $options[] = array(
                    'value' => $code,
                    'label' => $code
                );
            }
        }
            
        return $options;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if (is_array($value)) {
                if (in_array($option['value'], $value)) {
                    return $option['label'];
                }
            } else {
                if ($option['value'] == $value) {
                    return $option['label'];
                }
            }
        }
        return false;
    }

}
