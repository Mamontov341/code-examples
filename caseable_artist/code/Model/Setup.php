<?php

/**
 * Caseable artist entity setup
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Model_Setup extends Mage_Eav_Model_Entity_Setup
{
    /**
     * Scope store
     */
    const SCOPE_STORE = 0;

    /**
     * Scope global
     */
    const SCOPE_GLOBAL = 1;

    /**
     * Scope website
     */
    const SCOPE_WEBSITE = 2;

    /**
     * Install default entities
     *
     * @return array
     */
    public function getDefaultEntities()
    {
        return array(
            Caseable_Artist_Model_Artist::ENTITY => array(
                'entity_model' => 'artist/artist',
                'table' => 'artist/artist',
                'attribute_model' => 'catalog/resource_eav_attribute',
                'additional_attribute_table' => 'catalog/eav_attribute',
                'default_group' => 'General Information',
                'attributes' => array(
                    'prefix' => array(
                        'type' => 'varchar',
                        'label' => 'Prefix',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 10,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'firstname' => array(
                        'type' => 'varchar',
                        'label' => 'First Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 20,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'middlename' => array(
                        'type' => 'varchar',
                        'label' => 'Middle Name/Initial',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 30,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'lastname' => array(
                        'type' => 'varchar',
                        'label' => 'Last Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 40,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'suffix' => array(
                        'type' => 'varchar',
                        'label' => 'Suffix',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 50,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'company' => array(
                        'type' => 'varchar',
                        'label' => 'Company',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'street' => array(
                        'type' => 'varchar',
                        'label' => 'Street Address',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 70,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'city' => array(
                        'type' => 'varchar',
                        'label' => 'City',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 80,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                        'unique' => true,
                    ),
                    'country_id' => array(
                        'type' => 'varchar',
                        'label' => 'Country',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 90,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'region_id' => array(
                        'type' => 'varchar',
                        'label' => 'State/Province',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 100,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'postcode' => array(
                        'type' => 'varchar',
                        'label' => 'Zip/Postal Code',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 110,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'telephone' => array(
                        'type' => 'varchar',
                        'label' => 'Telephone',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 120,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'fax' => array(
                        'type' => 'varchar',
                        'label' => 'Fax',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 130,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'vat_id' => array(
                        'type' => 'varchar',
                        'label' => 'VAT number',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 140,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'paypal_account' => array(
                        'type' => 'varchar',
                        'label' => 'Paypal Account',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 140,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'Payment Details',
                    ),
                    'artist_active' => array(
                        'type' => 'static',
                        'label' => 'Active',
                        'input' => 'select',
                        'source' => 'eav/entity_attribute_source_boolean',
                        'sort_order' => 150,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'sku_pattern' => array(
                        'type' => 'static',
                        'label' => 'SKU Pattern',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 150,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'General Information',
                    ),
                    'updated_at' => array(
                        'type' => 'static',
                        'input' => 'text',
                        'backend' => 'eav/entity_attribute_backend_time_updated',
                        'sort_order' => 20,
                        'visible' => false,
                    ),
                    'profile_name' => array(
                        'type' => 'varchar',
                        'label' => 'Profile Name',
                        'input' => 'text',
                        'required' => true,
                        'sort_order' => 10,
                        'global' => self::SCOPE_STORE,
                        'group' => 'Profile',
                        'unique' => true,
                    ),
                    'profile_country' => array(
                        'type' => 'varchar',
                        'label' => 'Country',
                        'input' => 'select',
                        'source' => 'customer/entity_address_attribute_source_country',
                        'sort_order' => 20,
                        'global' => self::SCOPE_GLOBAL,
                        'group' => 'Profile',
                    ),
                    'banner_image' => array(
                        'type' => 'varchar',
                        'label' => 'Banner image',
                        'input' => 'file',
                        'required' => false,
                        'sort_order' => 40,
                        'global' => self::SCOPE_STORE,
                        'group' => 'Profile',
                    ),
                    'image' => array(
                        'type' => 'varchar',
                        'label' => 'Profile image',
                        'input' => 'file',
                        'required' => false,
                        'sort_order' => 50,
                        'global' => self::SCOPE_STORE,
                        'group' => 'Profile',
                    ),
                    'meta_title' => array(
                        'type' => 'varchar',
                        'label' => 'Page Title',
                        'class' => 'validate-length maximum-length-67',
                        'validate_rules' => 'a:2:{s:15:"max_text_length";i:67;s:15:"min_text_length";i:1;}',
                        'note' => 'Maximum 67 chars',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => self::SCOPE_STORE,
                        'group' => 'Profile',
                    ),
                    'meta_description' => array(
                        'type' => 'text',
                        'label' => 'Page Description',
                        'class' => 'validate-length maximum-length-155',
                        'validate_rules' => 'a:2:{s:15:"max_text_length";i:155;s:15:"min_text_length";i:1;}',
                        'note' => 'Maximum 155 chars',
                        'input' => 'textarea',
                        'required' => false,
                        'sort_order' => 80,
                        'global' => self::SCOPE_STORE,
                        'group' => 'Profile',
                    ),
                    'frontend_position' => array(
                        'type' => 'int',
                        'input' => 'text',
                        'sort_order' => 20,
                        'visible' => false,
                    ),
                ),
            ),
        );
    }

    /**
     * Prepare attribute values to save
     *
     * @param array $attr
     * @return array
     */
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'frontend_input_renderer' => $this->_getValue($attr, 'input_renderer'),
            'is_global' => $this->_getValue(
                    $attr,
                    'global',
                    self::SCOPE_GLOBAL
                ),
            'is_visible' => $this->_getValue($attr, 'visible', 1),
            'is_searchable' => $this->_getValue($attr, 'searchable', 0),
            'is_filterable' => $this->_getValue($attr, 'filterable', 0),
            'is_comparable' => $this->_getValue($attr, 'comparable', 0),
            'is_visible_on_front' => $this->_getValue($attr, 'visible_on_front', 0),
            'is_wysiwyg_enabled' => $this->_getValue($attr, 'wysiwyg_enabled', 0),
            'is_html_allowed_on_front' => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
            'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
            'is_filterable_in_search' => $this->_getValue($attr, 'filterable_in_search', 0),
            'used_in_product_listing' => $this->_getValue($attr, 'used_in_product_listing', 0),
            'used_for_sort_by' => $this->_getValue($attr, 'used_for_sort_by', 0),
            'apply_to' => $this->_getValue($attr, 'apply_to'),
            'position' => $this->_getValue($attr, 'position', 0),
            'is_configurable' => $this->_getValue($attr, 'is_configurable', 1),
            'is_used_for_promo_rules' => $this->_getValue($attr, 'used_for_promo_rules', 0)
        ));
        return $data;
    }

    /**
     * Create store based eav entity tables
     *
     * @param $baseTableName
     * @param array $options
     * @return $this
     */
    public function createStoreBasedEntityTables($baseTableName, array $options = array())
    {
        parent::createEntityTables($baseTableName, $options);
        $isNoDefaultTypes = $this->_getValue($options, 'no-default-types', false);
        $customTypes = $this->_getValue($options, 'types', array());

        $types = array();
        if (!$isNoDefaultTypes) {
            $types = array(
                'datetime' => array(Varien_Db_Ddl_Table::TYPE_DATETIME, null),
                'decimal' => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4'),
                'int' => array(Varien_Db_Ddl_Table::TYPE_INTEGER, null),
                'text' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '64k'),
                'varchar' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255'),
                'char' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255')
            );
        }

        if (!empty($customTypes)) {
            foreach ($customTypes as $type => $fieldType) {
                $types[$type] = $fieldType;
            }
        }

        foreach (array_keys($types) as $type) {
            $this->getConnection()->addIndex(
                $this->getTable(array($baseTableName, $type)),
                $this->getIdxName(
                    array($baseTableName, $type),
                    array('entity_id', 'attribute_id', 'store_id'),
                    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id', 'store_id'),
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
        }

        return $this;
    }
}