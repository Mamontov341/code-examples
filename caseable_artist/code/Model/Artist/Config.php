<?php

/**
 * Caseable artist config
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   1.0.0
 */
final class Caseable_Artist_Model_Artist_Config implements Mage_Media_Model_Image_Config_Interface
{
    /**
     * Retrieve upload base url for files
     *
     * @return string
     */
    final public function getBaseUploadUrl()
    {
        return Mage::getBaseUrl('media') . 'upload';
    }

    /**
     * Retrieve upload base path for files
     *
     * @return string
     */
    final public function getBaseUploadPath()
    {
        return Mage::getBaseDir('media') . DS . 'upload';
    }

    /**
     * Retrieve base url for artist media files
     *
     * @return string
     */
    final public function getBaseMediaUrl()
    {
        return Mage::getBaseUrl('media') . 'artist';
    }

    /**
     * Retrieve url for artist media file
     *
     * @param string $file
     * @return string
     */
    final public function getUploadUrl($file)
    {
        return $this->getBaseUploadUrl() . '/' . $file;
    }

    /**
     * Retrieve file system path for artist media file
     *
     * @param string $file
     * @return string
     */
    function getUploadPath($file)
    {
        return $this->getBaseUploadPath() . DS . $file;
    }

    /**
     * Retrieve base path for artist media files
     *
     * @return string
     */
    final public function getBaseMediaPath()
    {
        return Mage::getBaseDir('media') . DS . 'artist';
    }

    /**
     * Retrieve url for artist media file
     *
     * @param string $file
     * @return string
     */
    final public function getMediaUrl($file)
    {
        return $this->getBaseMediaUrl() . '/' . $file;
    }

    /**
     * Retrieve file system path for artist media file
     *
     * @param string $file
     * @return string
     */
    function getMediaPath($file)
    {
        return $this->getBaseMediaPath() . DS . $file;
    }
}