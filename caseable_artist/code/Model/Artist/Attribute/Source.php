<?php

/**
 * Caseable artist source model
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Model_Artist_Attribute_Source extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * @var null
     */
    private $_gridFilterArray = null;

    /**
     * Get all options
     *
     * @param bool $withEmpty
     * @return array
     */
    public function getAllOptions($withEmpty = false)
    {
        if (is_null($this->_options)) {
            $this->_options = array();
            $collection = Mage::getModel('artist/artist')->getCollection();
            $collection->addAttributeToSelect('profile_name');
            #$collection->addSortedNameToSelect();
            foreach ($collection as $artist) {
                $this->_options[] = array(
                    'label' => $artist->getProfileName(),
                    'value' => (int)$artist->getId(),
                );
            }
        }

        if ($withEmpty) {
            array_unshift($this->_options, array('value' => '', 'label' => ''));
        }

        return $this->_options;
    }

    /**
     * To option array
     *
     * @param bool $withEmpty
     * @return array
     */
    public function toOptionArray($withEmpty = false)
    {
        return $this->getAllOptions($withEmpty);
    }

    /**
     * Add value sort to collection
     *
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @param string $dir
     * @return $this|Mage_Eav_Model_Entity_Attribute_Source_Abstract
     */
    public function addValueSortToCollection($collection, $dir = 'asc')
    {
        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
        $valueTable1 = $this->getAttribute()->getAttributeCode() . '_t1';
        $valueTable2 = $this->getAttribute()->getAttributeCode() . '_t2';
        $collection->getSelect()->joinLeft(
            array($valueTable1 => $this->getAttribute()->getBackend()->getTable()),
            "`e`.`entity_id`=`{$valueTable1}`.`entity_id`"
            . " AND `{$valueTable1}`.`attribute_id`='{$this->getAttribute()->getId()}'"
            . " AND `{$valueTable1}`.`store_id`='{$adminStore}'",
            array()
        );
        if ($collection->getStoreId() != $adminStore) {
            $collection->getSelect()->joinLeft(
                array($valueTable2 => $this->getAttribute()->getBackend()->getTable()),
                "`e`.`entity_id`=`{$valueTable2}`.`entity_id`"
                . " AND `{$valueTable2}`.`attribute_id`='{$this->getAttribute()->getId()}'"
                . " AND `{$valueTable2}`.`store_id`='{$collection->getStoreId()}'",
                array()
            );
            $valueExpr = new Zend_Db_Expr("IF(`{$valueTable2}`.`value_id`>0, `{$valueTable2}`.`value`, `{$valueTable1}`.`value`)");
        } else {
            $valueExpr = new Zend_Db_Expr("`{$valueTable1}`.`value`");
        }
        $collection->getSelect()
            ->order($valueExpr, $dir);
        return $this;
    }

    /**
     * To grid filter array
     *
     * @return array|null
     */
    public function toGridFilterArray($withEmpty = false)
    {
        if ($this->_gridFilterArray === null) {
            $this->_gridFilterArray = array();
            if ($withEmpty) {
                $this->_gridFilterArray[] = '';
            }
            foreach ($this->getAllOptions() as $option) {
                $this->_gridFilterArray[$option['value']] = $option['label'];
            }
        }
        return $this->_gridFilterArray;
    }

    /**
     * Get flat columns
     *
     * @return array
     */
    public function getFlatColumns()
    {
        $columns = array();
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $isMulti = $this->getAttribute()->getFrontend()->getInputType() == 'multiselect';

        if (Mage::helper('core')->useDbCompatibleMode()) {
            $columns[$attributeCode] = array(
                'type'      => $isMulti ? 'varchar(255)' : 'int',
                'unsigned'  => false,
                'is_null'   => true,
                'default'   => null,
                'extra'     => null
            );
            if (!$isMulti) {
                $columns[$attributeCode . '_value'] = array(
                    'type'      => 'varchar(255)',
                    'unsigned'  => false,
                    'is_null'   => true,
                    'default'   => null,
                    'extra'     => null
                );
            }
        } else {
            $type = ($isMulti) ? Varien_Db_Ddl_Table::TYPE_TEXT : Varien_Db_Ddl_Table::TYPE_INTEGER;
            $columns[$attributeCode] = array(
                'type'      => $type,
                'length'    => $isMulti ? '255' : null,
                'unsigned'  => false,
                'nullable'   => true,
                'default'   => null,
                'extra'     => null,
                'comment'   => $attributeCode . ' column'
            );
            if (!$isMulti) {
                $columns[$attributeCode . '_value'] = array(
                    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
                    'length'    => 255,
                    'unsigned'  => false,
                    'nullable'  => true,
                    'default'   => null,
                    'extra'     => null,
                    'comment'   => $attributeCode . ' column'
                );
            }
        }

        return $columns;
    }

    /**
     * Get flat update select
     *
     * @param int $store
     * @return null|Varien_Db_Select
     */
    public function getFlatUpdateSelect($store)
    {
        $attribute = $this->getAttribute();
        $valueAttribute = Mage::getModel('eav/entity_attribute')->loadByCode(Caseable_Artist_Model_Artist::ENTITY, 'profile_name');
        
        $resourceModel = Mage::getResourceModel('eav/entity_attribute');
        $adapter = $resourceModel->getReadConnection();


        $joinConditionTemplate = "%s.entity_id = %s.entity_id"
            ." AND %s.entity_type_id = ".$attribute->getEntityTypeId()
            ." AND %s.attribute_id = ".$attribute->getId()
            ." AND %s.store_id = %d";
        $joinCondition = sprintf($joinConditionTemplate,
            'e', 't1', 't1', 't1', 't1',
            Mage_Core_Model_App::ADMIN_STORE_ID);
        
        $joinValueConditionTemplate = "%s.entity_id = %s"
            ." AND %s.entity_type_id = ".$valueAttribute->getEntityTypeId()
            ." AND %s.attribute_id = ".$valueAttribute->getId()
            ." AND %s.store_id = %d";
        
        if ($attribute->getFlatAddChildData()) {
            $joinCondition .= ' AND e.child_id = t1.entity_id';
        }

        $valueExpr = $adapter->getCheckSql('t2.value_id > 0', 't2.value', 't1.value');

        /** @var $select Varien_Db_Select */
        $select = $adapter->select()
            ->joinLeft(
                array('t1' => $attribute->getBackend()->getTable()),
                $joinCondition,
                array())
            ->joinLeft(
                array('t2' => $attribute->getBackend()->getTable()),
                sprintf($joinConditionTemplate, 'e', 't2', 't2', 't2', 't2', $store),
                array($attribute->getAttributeCode() => $valueExpr));
        
        $valueIdExpr = $adapter->getCheckSql('to2.value_id > 0', 'to2.value', 'to1.value');
        $select
            ->joinLeft(array('to1' => $valueAttribute->getBackend()->getTable()),
                sprintf($joinValueConditionTemplate, 'to1', $valueExpr, 'to1', 'to1', 'to1', 'to1', Mage_Core_Model_App::ADMIN_STORE_ID),        
                        array())
            ->joinLeft(array('to2' => $valueAttribute->getBackend()->getTable()),
                sprintf($joinValueConditionTemplate, 'to2', $valueExpr, 'to2', 'to2', 'to2', 'to2', $store),        
                array($attribute->getAttributeCode() . '_value' => $valueIdExpr));
        
        if ($attribute->getFlatAddChildData()) {
            $select->where("e.is_child = ?", 0);
        }

        return $select;
    }
}