<?php

/**
 * Caseable artist image attribute backend model
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */
class Caseable_Artist_Model_Artist_Attribute_Backend_Image extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Before save method for image attribute
     * here we check if we need to move the file or delete it
     *
     * @param Varien_Object $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
     */
    public function beforeSave($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();

        if (true === $object->dataHasChangedFor($attrCode)) {
            $value = $object->getData($attrCode);
            $orgValue = $object->getOrigData($attrCode);

            if (true === empty($value)) {
                // Check if we need to delete the image
                if (false === empty($orgValue)) {
                    $this->_deleteImage($orgValue);
                }

                $object->setData($attrCode, false);
            } else if (false === empty($value)) {
                if (false === empty($orgValue)) {
                    $this->_deleteImage($orgValue);
                }

                $filename = $this->_saveImage($value, $object, $attrCode);
                $object->setData($attrCode, $filename);
            }
        }

        return parent::beforeSave($object);
    }

    /**
     * Get config
     *
     * @return Mage_Core_Model_Abstract
     */
    private function _getConfig()
    {
        return Mage::getSingleton('artist/artist_config');
    }

    /**
     * Remove image from disc
     *
     * @param $fileName
     */
    private function _deleteImage($fileName)
    {
        try {
            $file = $this->_getConfig()->getMediaPath($fileName);
            $filePath = dirname($file);
            $ioAdapter = new Varien_Io_File();
            $ioAdapter->cd($filePath);
            $ioAdapter->rm($fileName);
            $ioAdapter->close();
        } catch (Exception $e) {
            Mage::logException($e);
        };
    }

    /**
     * Move image from upload path to final directory
     *
     * @param $fileName
     * @return string
     */
    private function _saveImage($fileName, $object, $attrCode)
    {
        $artist = Mage::getModel('artist/artist')->load($object->getId());

        try {
            if ($attrCode == 'profile_image') {
                $newFilename = 'image.jpg';
            }
            if ($attrCode == 'profile_banner') {
                $newFilename = 'banner.jpg';
            }

            $ioObject = new Varien_Io_File();
            $ioObject->setAllowCreateFolders(true);
            $ioObject->open(array('path' => $this->_getConfig()->getBaseMediaPath() . DS . $artist->getUrlKey()));
            $ioObject->mv(
                $this->_getConfig()->getUploadPath($fileName),
                $this->_getConfig()->getMediaPath($artist->getUrlKey() . DS . $newFilename)
            );
        } catch (Exception $e) {
            Mage::throwException(
                Mage::helper('caseable_artist')->__('Failed to copy file %s -> %s.', $fileName, $artist->getUrlKey() . DS . $newFilename)
            );
        };

        return $newFilename;
    }
}