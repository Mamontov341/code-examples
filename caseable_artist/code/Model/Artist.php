<?php

/**
 * Caseable artist model
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Model_Artist extends Mage_Core_Model_Abstract
{
    /**
     * Entity name
     */
    const ENTITY = 'artist';

    /**
     * Cache tag
     */
    const CACHE_TAG = 'artist';

    /**
     * Registry key
     */
    const REGISTRY_KEY = 'artist';

    const ARTIST_THUMBNAILS_PATH          = "/artist/";
    const DRAFTS_ARTIST_THUMBNAILS_PATH     = "/draft/thumbnail/";
    const DRAFTS_ARTIST_FULL_IMAGE_PATH     = "/draft/type/";
    const CATALOG_PRODUCT_ARTIST_PATH     = "/catalog/product/";

    /**
     * Attribute set id
     *
     * @var int
     */
    protected $attributeSetId = null;

    /**
     * Attribute set
     *
     * @var Mage_Eav_Model_Entity_Attribute_Set
     */
    protected $attribute_set = null;

    /**
     * Cache tag as property
     *
     * @var string
     */
    protected $_cacheTag = array('artist');

    /**
     * Event prefix in observers
     *
     * @var string
     */
    protected $_eventPrefix = 'artist';

    /**
     * Event object name in observers
     *
     * @var string
     */
    protected $_eventObject = 'artist';

    /**
     * Attribute default values
     *
     * @var array
     */
    protected $_defaultValues = array();

    /**
     * This array contains codes of attributes which have value in current store
     *
     * @var array
     */
    protected $_storeValuesFlags = array();

    /**
     * Locked attributes
     *
     * @var array
     */
    protected $_lockedAttributes = array();

    /**
     * Is model deletable
     *
     * @var bool
     */
    protected $_isDeleteable = true;

    /**
     * Is model readonly
     *
     * @var bool
     */
    protected $_isReadonly = false;

    /**
     * Artist commision instance
     *
     * @var Caseable_Artist_Model_Commision
     */
    protected $_commisionInstance;

    /**
     * Artist constructor
     */
    protected function _construct()
    {
        $this->_init('artist/artist');
    }

    /**
     * Get artist resource model
     *
     * @return Mage_Core_Model_Mysql4_Abstract|object
     */
    protected function _getResource()
    {
        return Mage::getResourceSingleton('artist/artist');
    }

    /**
     * Validate artist data
     *
     * @return array|bool
     */
    public function validate()
    {
        Mage::dispatchEvent($this->_eventPrefix . '_validate_before', array($this->_eventObject => $this));
        $errors = $this->_getResource()->validate($this);
        Mage::dispatchEvent($this->_eventPrefix . '_validate_after', array($this->_eventObject => $this));

        return $errors;
    }

    /**
     * Retrieve Store Id
     *
     * @return int|mixed
     */
    public function getStoreId()
    {
        if (true === $this->hasData('store_id')) {
            return $this->getData('store_id');
        }

        return Mage::app()->getStore()->getId();
    }

    /**
     * Set original loaded data if needed
     *
     * @param null $key
     * @param null $data
     * @return $this|Varien_Object
     */
    public function setOrigData($key = null, $data = null)
    {
        if (true === Mage::app()->getStore()->isAdmin()) {
            return parent::setOrigData($key, $data);
        }

        return $this;
    }

    /**
     * Get attribute set Id
     *
     * @return int
     */
    public function getAttributeSetId()
    {
        if ($this->attributeSetId === null) {
            $this->attributeSetId = Mage::getModel('eav/entity')->setType(self::ENTITY)->getTypeId();
        }
        return $this->attributeSetId;
    }

    /**
     * Lock attribute
     *
     * @param $attributeCode
     * @return $this
     */
    public function lockAttribute($attributeCode)
    {
        $this->_lockedAttributes[$attributeCode] = true;
        return $this;
    }

    /**
     * Unlock attribute
     *
     * @param $attributeCode
     * @return $this
     */
    public function unlockAttribute($attributeCode)
    {
        if ($this->isLockedAttribute($attributeCode)) {
            unset($this->_lockedAttributes[$attributeCode]);
        }

        return $this;
    }

    /**
     * Unlock all attributes
     *
     * @return $this
     */
    public function unlockAttributes()
    {
        $this->_lockedAttributes = array();
        return $this;
    }

    /**
     * Retrieve locked attributes
     *
     * @return array
     */
    public function getLockedAttributes()
    {
        return array_keys($this->_lockedAttributes);
    }

    /**
     * Checks that model have locked attributes
     *
     * @return bool
     */
    public function hasLockedAttributes()
    {
        return !empty($this->_lockedAttributes);
    }

    /**
     * Retrieve locked attributes
     *
     * @param $attributeCode
     * @return bool
     */
    public function isLockedAttribute($attributeCode)
    {
        return isset($this->_lockedAttributes[$attributeCode]);
    }

    /**
     * Overwrite data in the object
     *
     * @param array|string $key
     * @param null $value
     * @return $this|Varien_Object
     */
    public function setData($key, $value = null)
    {
        if ($this->hasLockedAttributes()) {
            if (is_array($key)) {
                foreach ($this->getLockedAttributes() as $attribute) {
                    if (isset($key[$attribute])) {
                        unset($key[$attribute]);
                    }
                }
            } elseif ($this->isLockedAttribute($key)) {
                return $this;
            }
        } elseif ($this->isReadonly()) {
            return $this;
        }

        return parent::setData($key, $value);
    }

    /**
     * Unset data from the object
     *
     * @param null $key
     * @return $this|Varien_Object
     */
    public function unsetData($key = null)
    {
        if ((!is_null($key) && $this->isLockedAttribute($key)) ||
            $this->isReadonly()
        ) {
            return $this;
        }

        return parent::unsetData($key);
    }

    /**
     * Get collection instance
     *
     * @return Caseable_Artist_Model_Resource_Artist_Collection
     */
    public function getResourceCollection()
    {
        $collection = parent::getResourceCollection()->setStoreId($this->getStoreId());

        return $collection;
    }

    /**
     * Load entity by attribute
     *
     * @param $attribute
     * @param $value
     * @param string $additionalAttributes
     * @return bool
     */
    public function loadByAttribute($attribute, $value, $additionalAttributes = '*')
    {
        $collection = $this->getResourceCollection()
            ->addAttributeToSelect($additionalAttributes)
            ->addAttributeToFilter($attribute, $value)
            ->setPage(1, 1);

        foreach ($collection as $object) {
            return $object;
        }
        return false;
    }

    /**
     * Retrieve store object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return Mage::app()->getStore($this->getStoreId());
    }

    /**
     * Retrieve all store ids of object current website
     *
     * @return array
     */
    public function getWebsiteStoreIds()
    {
        return $this->getStore()->getWebsite()->getStoreIds(true);
    }

    /**
     * Adding attribute code and value to default value registry
     * Default value existing is flag for using store value in data
     *
     * @param $attributeCode
     * @param $value
     * @return $this
     */
    public function setAttributeDefaultValue($attributeCode, $value)
    {
        $this->_defaultValues[$attributeCode] = $value;

        return $this;
    }

    /**
     * Retrieve default value for attribute code
     *
     * @param $attributeCode
     * @return bool
     */
    public function getAttributeDefaultValue($attributeCode)
    {
        return array_key_exists($attributeCode, $this->_defaultValues) ? $this->_defaultValues[$attributeCode] : false;
    }

    /**
     * Set attribute code flag if attribute has value in current store and does not use
     * value of default store as value
     *
     * @param $attributeCode
     * @return $this
     */
    public function setExistsStoreValueFlag($attributeCode)
    {
        $this->_storeValuesFlags[$attributeCode] = true;

        return $this;
    }

    /**
     * Check if object attribute has value in current store
     *
     * @param $attributeCode
     * @return bool
     */
    public function getExistsStoreValueFlag($attributeCode)
    {
        return array_key_exists($attributeCode, $this->_storeValuesFlags);
    }

    /**
     * Before save unlock attributes
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->unlockAttributes();

        Mage::getSingleton('index/indexer')->logEvent($this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE);

        return parent::_beforeSave();
    }

    /**
     * Checks model is deletable
     *
     * @return bool
     */
    public function isDeleteable()
    {
        return $this->_isDeleteable;
    }

    /**
     * Set is deletable flag
     *
     * @param $value
     * @return $this
     */
    public function setIsDeleteable($value)
    {
        $this->_isDeleteable = (bool)$value;
        return $this;
    }

    /**
     * Checks model is deletable
     *
     * @return bool
     */
    public function isReadonly()
    {
        return $this->_isReadonly;
    }

    /**
     * Set is deletable flag
     *
     * @param $value
     * @return $this
     */
    public function setIsReadonly($value)
    {
        $this->_isReadonly = (bool)$value;
        return $this;
    }

    /**
     * Retrieve entity type attributes
     *
     * @return mixed
     */
    public function getEditableAttributes()
    {
        $cacheKey = '_cache_editable_attributes';

        if (false === $this->hasData($cacheKey)) {
            $editableAttributes = array();

            foreach ($this->getSetAttributes() as $attributeCode => $attribute) {
                $editableAttributes[$attributeCode] = $attribute;
            }

            $this->setData($cacheKey, $editableAttributes);
        }

        return $this->getData($cacheKey);
    }


    /**
     * Get array of set attributes
     *
     * @return mixed
     */
    public function getSetAttributes()
    {
        return $this->getResource()->loadAllAttributes($this)->getSortedAttributes($this->getAttributeSetId());
    }

    /**
     * Retrieve attributes
     *
     * @param null $groupId
     * @return array|mixed
     */
    public function getAttributes($groupId = null)
    {
        $entityAttributes = $this->getEditableAttributes($this);
        if ($groupId) {
            $attributes = array();
            foreach ($entityAttributes as $attribute) {
                if ($attribute->getAttributeGroupId() == $groupId) {
                    $attributes[] = $attribute;
                }
            }
        } else {
            $attributes = $entityAttributes;
        }

        return $attributes;
    }
    
    protected function _afterSave()
    {
        $this->getCommisionInstance()->saveCommisions($this);
        $result = parent::_afterSave();
        return $result;
    }    
    
    /**
     * Retrieve commision instance
     *
     * @return  Caseable_Artist_Model_Commision
     */
    public function getCommisionInstance()
    {
        if (!$this->_commisionInstance) {
            $this->_commisionInstance = Mage::getSingleton('artist/commision');
        }
        return $this->_commisionInstance;
    }

    /**
     * Change the names of the directories when the artist name is changed.
     *
     */
    public function changeDirectoryName($artistName)
    {
        $artistUrlKey = $this->getUrlKey();
        $imagePath = strtolower($artistName);
        $imagePath = preg_replace('#[ -]+#', '-', $imagePath);
        $base_dir = "/mnt/nfs/media/media";  // should refer directly to /mnt/nfs/media/media, and not to "media" symlink.
        $path = "";
        $errorMessages = array();

        if ($imagePath != $artistUrlKey) {
            $path = $base_dir . self::ARTIST_THUMBNAILS_PATH;
            $finalPath = $path . $artistUrlKey;
            $finalNewPath = $path . $imagePath;
            $response = rename($finalPath, $finalNewPath);

            if(!$response){
                $errorMessages[] = "Cannot rename a directory " . $path . $artistUrlKey;
            }

            $path = $base_dir . self::DRAFTS_ARTIST_THUMBNAILS_PATH;
            $response = rename($path . $artistUrlKey, $path . $imagePath);

            if(!$response){
                $errorMessages[] = "Cannot rename a directory " . $path . $artistUrlKey;
            }
            $path = $base_dir . self::DRAFTS_ARTIST_FULL_IMAGE_PATH;
            $response = rename($path . $artistUrlKey, $path . $imagePath);

            if(!$response){
                $errorMessages[] = "Cannot rename a directory " . $path . $artistUrlKey;
            }


            $sizes = Mage::getModel('rawproduct/size')->getCollection();
            $sizes->addAttributeToSelect('url_key');
            foreach ($sizes as $size) {
                $sizeUrlKey = $size->getUrlKey();
                $path = $base_dir . self::CATALOG_PRODUCT_ARTIST_PATH . $sizeUrlKey . "/";

                if (is_dir($path)) {
                    $results = scandir($path);
                    if ($results) {
                        foreach ($results as $result) {
                            if ($result === '.' or $result === '..') continue;

                            if (is_dir($path . '/' . $result)) {
                                if ($result == $artistUrlKey) {
                                    if ($result != $imagePath) {
                                        $oldName = $path . $result;
                                        $response = rename($oldName, $path . $imagePath);
                                        if(!$response){
                                            $errorMessages[] = "Cannot rename a directory " . $oldName;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $errorMessages;
        }
    }
}