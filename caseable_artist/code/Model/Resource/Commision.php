<?php

class Caseable_Artist_Model_Resource_Commision extends Mage_Core_Model_Resource_Db_Abstract {
    const DEFAULT_COMMISION = 6;
    protected $_defaultCommisions = array();

    protected function _construct() {
        $this->_init('artist/commision', 'entity_id');
        
        $this->_defaultCommisions = array(
            Caseable_Core_Model_Channel::TYPE_MARKETPLACE => self::DEFAULT_COMMISION, 
            Caseable_Core_Model_Channel::TYPE_APP => self::DEFAULT_COMMISION,
            Caseable_Core_Model_Channel::TYPE_WEBSITE => 10
        );
    }

    public function getCommisionsByArtist($artistEntId) {
        $read = $this->_getReadAdapter();

        $select = $read->select()
                ->from($this->getTable('artist/commision'))
                ->where("artist_entity_id =" . $artistEntId);

        return $read->fetchAll($select);
    }

    public function getCommisionByArtistChannel($artistEntId, $channelId) {
        $read = $this->_getReadAdapter();

        $select = $read->select()
                ->from($this->getTable('artist/commision'), 'commision')
                ->where('channel_id = :channel_id')
                ->where('artist_entity_id = :artist_entity_id')
        ;
        $bind = array(':channel_id' => (int)$channelId, ':artist_entity_id' =>  (int)$artistEntId);

        return $read->fetchOne($select, $bind);
    }
    
    public function getDefautCommisions(){
        return $this->_defaultCommisions;
    }    
    
    public function saveCommisions($artist, $data) {
        $adapter    = $this->_getWriteAdapter();
        if (!is_array($data)) {
            $data = array();
        }
        
        $links          = array();
        $linksEntIds    = array();
        
        $bind   = array( ':artist_entity_id'    => (int)$artist->getId());
        $selectCommision = $adapter->select()
            ->from($this->getMainTable(), array('channel_id', 'commision'))
            ->where('artist_entity_id = :artist_entity_id');
        
        $selectEntId = $adapter->select()
            ->from($this->getMainTable(), array('channel_id', 'entity_id'))
            ->where('artist_entity_id = :artist_entity_id');

        
        $links          = $adapter->fetchPairs($selectCommision, $bind);
        $linksEntIds    = $adapter->fetchPairs($selectEntId, $bind);
        
        /*
        $deleteIds = array();
        if (!empty($deleteIds)) {
            $adapter->delete($this->getMainTable(), array(
                'entity_id IN (?)' => $deleteIds,
            ));
        }
        */
        
        foreach ($data as $channelId => $commisionValue) {
            $dbCommision    = null; 
            $entityId       = null;
            if (isset($links[$channelId])) {
                $dbCommision = $links[$channelId];
                $entityId = $linksEntIds[$channelId];
                
                if($dbCommision != $commisionValue){

                    if($commisionValue==''){

                        $bindChannel   = array(':channel_id' => $channelId);
                        $resource = Mage::getSingleton("core/resource");
                        
                        $selectChannel = $adapter->select()
                            ->from($this->getTable("caseable_core/channel"), array('channel_id', 'type'))
                            ->where('channel_id = :channel_id');

                        $linksChannel   = $adapter->fetchPairs($selectChannel, $bindChannel);
                        
                        if(isset($linksChannel[$channelId])){
                            $typeChannel = $linksChannel[$channelId];
                            $value = $this->getDefaultCommisionByChannelType($typeChannel);
                            $commisionValue = $value;
                        }
                    }
                    
                    $bind = array(
                        'channel_id'       => $channelId,
                        'artist_entity_id'  => $artist->getId(),
                        'commision'         => $commisionValue
                    );   
                    $condition = array(
                        'entity_id = ?' => (int) $entityId,
                    );

                    $adapter->update($this->getMainTable(), $bind, $condition);
                }
                unset($links[$channelId]);
            } else {
                $bind = array(
                    'channel_id'       => $channelId,
                    'artist_entity_id'  => $artist->getId(),
                    'commision'         => $commisionValue
                );

                $adapter->insert($this->getMainTable(), $bind);
                $linkId = $adapter->lastInsertId($this->getMainTable());
            }
        }

        return $this;
    } 
    
    public function getDefaultCommisionByChannelType($type){
        $defaultCommision = $this->getDefaultCommisionValue();
        $data = $this->getDefautCommisions();
        if(isset($data[$type])){
            $defaultCommision = $data[$type];
        }
        return $defaultCommision;
    }
    
    public function getDefaultCommisionValue(){
        return self::DEFAULT_COMMISION;
    }

}
