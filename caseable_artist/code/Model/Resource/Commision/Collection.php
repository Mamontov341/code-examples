<?php

class Caseable_Artist_Model_Resource_Commision_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected $_joinedFields = array();

    protected function _construct() { 
        $this->_init('artist/commision');        
    }
}