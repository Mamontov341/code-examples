<?php

/**
 * Caseable artist adminhtml controller
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Adminhtml_Caseable_ArtistController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init action
     *
     * @return Caseable_Artist_Adminhtml_Caseable_ArtistController
     */
    protected function _initAction()
    {
        $this->loadLayout();
        return $this;
    }

    /**
     * WYSIWYG editor action for ajax request
     *
     */
    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id'          => $storeId,
            'store_media_url'   => $storeMediaUrl,
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    /**
     * Current / empty artist object
     *
     * @return Caseable_Artist_Model_Artist
     */
    protected function _initArtist()
    {
        $artistId = (int)$this->getRequest()->getParam('id');

        $artistModel = Mage::getModel('artist/artist')->setStoreId($this->getRequest()->getParam('store', 0));
        $artistModel->setData('_edit_mode', true);

        if ($artistId > 0) {
            $artistModel->load($artistId);
        }

        Mage::register(Caseable_Artist_Model_Artist::REGISTRY_KEY, $artistModel);

        return $artistModel;
    }

    /**
     * Index view
     *
     * @view adminhtml_caseable_artist_index
     */
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * index view
     * @view adminhtml_caseable_artist_positions
     */
    public function positionsAction()
    {
        $this->_initAction()->renderLayout();
    }

    /**
     * Grid view
     *
     * @view adminhtml_caseable_artist_grid
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridPositionsAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Create new view
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit view
     *
     * @view adminhtml_caseable_artist_edit
     */
    public function editAction()
    {
        $artistModel = $this->_initArtist();

        if ($artistModel && false === $artistModel->getId()) {
            $this->_getSession()->addError($this->__('This Artist no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        $this->_title($artistModel->getId() ? $artistModel->getName() : $this->__('New Artist'));
        if ($artistModel->getId()) {
            if ($this->getRequest()->getParam('store', 0) > 0) {
                /** here we can lock attributes */
            }
        }

        $data = Mage::getSingleton('adminhtml/session')->getArtistData(true);
        if (false === empty($data)) {
            $artistModel->addData($data);
        }

        $this->_initAction()->renderLayout();
    }

    /*
     * return in format array( array('channel_id' => 'commision_value') )
     */
    protected function prepareCommisionData(){
        $commisionsArr = array();
        
        $commisions = $this->getRequest()->getPost('commision');
        $channels   = $this->getRequest()->getPost('channel_id');

        for($i=0,$n=count($commisions); $i<$n; $i++){
            $commisionValue = trim($commisions[$i]);
            $channelId     = $channels[$i];

            if(!is_null($commisionValue) && !is_null($channelId)){
                $commisionsArr[$channelId] = $commisionValue;
            }
        }   

        return $commisionsArr; 
    }
    /**
     * Save action
     */
    public function saveAction()
    {
        $artistModel = $this->_initArtist();
        $artistId = $artistModel->getId();
        $redirectBack = $this->getRequest()->getParam('back', false);
        $data = $this->getRequest()->getPost('artist');


        if (true === is_array($data) || is_array($this->getRequest()->getPost('use_default'))) {
            if (false === is_array($data)) {
                $data = array();
            }

            $artistModel->addData($data);
            $artistModel->setCommisionsData($this->prepareCommisionData());

            $url_key = Mage::getSingleton('catalog/product_url')->formatUrlKey($artistModel->getProfileName());

            // change a name of directory on save
            $resp = $artistModel->changeDirectoryName($data["profile_name"]);
            $artistModel->setUrlKey($url_key);

            if(!$artistModel->getFrontendPosition()) {
                $artistModel->setFrontendPosition(1);
            }
            
            // Check "Use Default Value" checkboxes values
            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $artistModel->setData($attributeCode, false);
                }
            }

            try {
                if (false === Mage::getSingleton('admin/session')->isAllowed('admin/caseable/artist_menue/artist/edit')) {
                    throw new Exception('You don\'t have the permission to edit a artist', 0);
                }

                $artistModel->validate();
                $artistModel->save();
                $artistId = $artistModel->getId();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__(sprintf('The artist %s has been updated. ', $artistModel->getName())));
                $error_str = "";
                $count = 1;
                foreach ($resp as $message) {
                    $error_str .= $message . "\n </br>";
                        if($count == 8)
                        {
                            break;
                        }
                    $count++;
                }

                if($error_str != ""){
                    Mage::getSingleton('adminhtml/session')->addError($error_str);
                }
                Mage::getSingleton('adminhtml/session')->getArtistData(true);
            } catch (Mage_Eav_Model_Entity_Attribute_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setArtistData($data);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Whoops, our bad... Something has gone horribly wrong.'));
                Mage::getSingleton('adminhtml/session')->setTypeData($data);
                $redirectBack = true;
            }

            $this->_redirectReferer();
        }

        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id' => $artistId,
                '_current' => true
            ));
        } else {
            $this->_redirect('*/*/', array('store' => $this->getRequest()->getParam('store', 0)));
        }
    }

    /**
     * Save positions action
     */
    public function savePositionsAction()
    {

        $data = $this->getRequest()->getParams();

        $artistModel = Mage::getModel('artist/artist');
        foreach($data["positions"] as $id => $position) {

            $artistId = (int)substr($id, 3);

            if ($artistId > 0) {
                $artist = $artistModel->load($artistId);

                if($artist) {
                    try {
                        $artist->setFrontendPosition($position);
                        //$artist->setStoreId($data["store"]);
                        $artist->save();
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    }
                }
            }

        }

    }

    /**
     * Save positions action
     */
    public function saveMenuPositionsAction()
    {

        $data = $this->getRequest()->getParams();

        $artistModel = Mage::getModel('artist/artist');
        foreach($data["positions"] as $id => $position) {

            $artistId = (int)substr($id, 3);

            if ($artistId > 0) {
                $artist = $artistModel->load($artistId);

                if($artist) {
                    try {
                        $artist->setMenuPosition($position);
                        $artist->save();
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    }
                }
            }

        }

    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $artistModel = $this->_initArtist();

        if ($artistModel->getId() > 0) {
            $typeName = $artistModel->getName();

            try {
                if (false === Mage::getSingleton('admin/session')->isAllowed('admin/artist/delete')) {
                    throw new Exception('You don\'t have the permission to delete a artist', 0);
                }

                $artistModel->delete();
                $this->_getSession()->addSuccess($this->__(sprintf('The artist %s has been deleted.', $typeName)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->getResponse()->setRedirect($this->getUrl('*/*/', array('store' => $this->getRequest()->getParam('store'))));
    }

    /**
     * Mass delete action
     */
    public function massDeleteAction()
    {
        $artistIds = $this->getRequest()->getParam('artist');

        if(!is_array($artistIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select artist(s).'));
        } else {
            try {
                $artist = Mage::getModel('artist/artist');

                foreach ($artistIds as $artistId) {
                    $artist->load($artistId)
                        ->delete();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were deleted.', count($artistIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Update status action
     */
    public function massStatusAction()
    {
        $artistIds = (array)$this->getRequest()->getParam('artist');
        $storeId   = (int)$this->getRequest()->getParam('store', 0);
        $status    = (int)$this->getRequest()->getParam('status');

        try {
            foreach ($artistIds as $artistId) {
                $artist = Mage::getModel('artist/artist')->load($artistId);
                if ($artist) {
                    $artist->setArtistActive($status);
                    $artist->save();
                }
            }

            $this->_getSession()->addSuccess(
                $this->__('Total of %d record(s) have been updated.', count($artistIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('An error occurred while updating the status.'));
        }

        $this->_redirect('*/*/', array('store' => $storeId));
    }

    /**
     * Validate artist via ajax
     */
    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);
        $artistModel = $this->_initArtist();
        $data = $this->getRequest()->getPost('artist');

        if (true === is_array($data) || is_array($this->getRequest()->getPost('use_default'))) {
            if (false === is_array($data)) {
                $data = array();
            }

            $artistModel->addData($data);

            // Check "Use Default Value" checkboxes values
            if ($useDefaults = $this->getRequest()->getPost('use_default')) {
                foreach ($useDefaults as $attributeCode) {
                    $artistModel->setData($attributeCode, false);
                }
            }

            try {
                $artistModel->validate();
            } catch (Mage_Eav_Model_Entity_Attribute_Exception $e) {
                $response->setError(true);
                $response->setAttribute($e->getAttributeCode());
                $response->setMessage($e->getMessage());
            } catch (Mage_Core_Exception $e) {
                $response->setError(true);
                $response->setMessage($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_initLayoutMessages('adminhtml/session');

                $response->setError(true);
                $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
            }
        }

        $this->getResponse()->setBody($response->toJson());
    }
 
    
    public function commisionAction()
    {   $artistModel = $this->_initArtist();
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function commisionGridAction()
    {       
        $artistModel = $this->_initArtist();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('caseable/artists');
    }

}