<?php

/**
 * Caseable artist helper
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCurrencyRateByCode($currencyTo, $currencyFrom = '')
    {
        if ($currencyTo == null) {
            $currencyTo = 'EUR';
        }

        $rate = 1;

        try {
            if (empty($currencyFrom)) {
                $currencyFrom = Mage::app()->getStore()->getBaseCurrencyCode();
            }
            
            if ($currencyTo == $currencyFrom) {
                return $rate;
            }
            
            $rate = (float) Mage::getModel('directory/currency')
                    ->load($currencyFrom)
                    ->getAnyRate($currencyTo); 
        } catch(Exception $e) {
            Mage::logException($e);
        }

        return $rate;
    }

    public function getArtistCommisionPersent($artistId, $store)
    {
        $percentValue = 0;
        $artist = Mage::getModel('artist/artist')->setStore($store)->load($artistId);

        if ($artist && $artist->getData('entity_id')) {
            $percentValue = $artist->getData('commision_percent');
        }

        return $percentValue;
    }

    public function getArtistCommisionCurrency($artistId, $store) {

        $artist = Mage::getModel('artist/artist')->setStore($store)->load($artistId);

        if ($artist && $artist->getData('entity_id')) {
            $currencyCode = $artist->getData('commision_currency');
        } else {
            $currencyCode = Mage::app()->getBaseCurrencyCode();
        }

        return $currencyCode;
    }
}