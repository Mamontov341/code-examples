<?php
/**
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 04.01.2016
 * Time: 12:33
 */

$installer = $this;

$installer->startSetup();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$installer->getConnection()->addColumn(
    $installer->getTable('artist/artist'),
    'menu_visible',
    array(
        'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned' => true,
        'default'  => 0,
        'comment'  => 'Is Visible in Drop-down menu'
    )
);

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'menu_visible', array(
    'type' => 'static',
    'label' => 'Visible in Drop-down menu',
    'input' => 'select',
    'sort_order' => 100,
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => SCOPE_GLOBAL,
    'group' => 'Artist Profile',
    'required' => false,
));

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'menu_position', array(
    'type' => 'int',
    'label' => 'Position in Drop-down menu',
    'input' => 'text',
    'required' => false,
    'sort_order' => 110,
    'global' => SCOPE_STORE,
    'group' => 'Artist Profile',
));

$installer->getConnection()->resetDdlCache();
$installer->endSetup();