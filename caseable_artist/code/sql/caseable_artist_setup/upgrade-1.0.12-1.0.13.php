<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('artist/commision');

if ($installer->getConnection()->isTableExists($tableName)) {

$installer->getConnection()
        ->addIndex(
                $tableName, 
                $installer->getIdxName('artist/commision', array('frontend_id', 'artist_entity_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
                array('frontend_id', 'artist_entity_id'), 
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE   
        );
$installer->endSetup();
}