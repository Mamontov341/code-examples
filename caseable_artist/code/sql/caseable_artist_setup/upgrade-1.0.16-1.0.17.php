<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('artist/commision');

$installer->getConnection()->dropIndex(
    $tableName, 
    $installer->getIdxName('artist/commision', array('frontend_id', 'artist_entity_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
);

$installer->getConnection()->dropForeignKey(
    $tableName, 
    'FK_caseable_artist_entity_commision_frontend_id'
);

$installer->getConnection()->changeColumn(
    $tableName,
    'frontend_id',
    'channel_id',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
        'comment'  => 'Channel ID'
    )
);

$installer->getConnection()
    ->addIndex(
        $tableName, 
        $installer->getIdxName('artist/commision', array('channel_id', 'artist_entity_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('channel_id', 'artist_entity_id'), 
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE   
    );


//$installer->getConnection()
//    ->addForeignKey(
//        'FK_caseable_artist_entity_commision_channel_id',
//        $tableName,  
//        'channel_id',
//        $installer->getTable('channel/channel'),
//        'channel_id'
//    );

$installer->getConnection()->resetDdlCache();
$installer->endSetup();
