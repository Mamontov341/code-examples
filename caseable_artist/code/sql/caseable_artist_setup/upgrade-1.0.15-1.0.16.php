<?php

/**
 * rawproduct setup file
 *
 * @category  Caseable
 * @package   Caseable_Rawproduct
 * @author    David Bittner <david.bittner@caseable.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */

/* @var $installer Caseable_Rawproduct_Model_Setup */
$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'url_key', array(
    'type' => 'varchar',
    'label' => 'URL Key',
    'input' => 'text',
    'visible' => true,
    'required' => true,
    'sort_order' => 100,
    'global' => Caseable_Artist_Model_Setup::SCOPE_GLOBAL,
    'group' => 'General Information',
));

$installer->getConnection()->resetDdlCache();
$installer->endSetup();
