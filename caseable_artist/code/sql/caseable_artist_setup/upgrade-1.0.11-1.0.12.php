<?php

$installer = $this;
$installer->startSetup();

$installer->run(<<<SQL

DROP TABLE IF EXISTS caseable_artist_entity_commision;
CREATE TABLE caseable_artist_entity_commision (
  entity_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  frontend_id INT(11) UNSIGNED NOT NULL,
  artist_entity_id INT(10) UNSIGNED NOT NULL,
  commision decimal(12,2) NOT NULL default '6.00',
  PRIMARY KEY (entity_id),
  CONSTRAINT `FK_caseable_artist_entity_commision_frontend_id` FOREIGN KEY (`frontend_id`) REFERENCES frontend (`frontend_id`) ON DELETE CASCADE ON UPDATE CASCADE,      
  CONSTRAINT `FK_caseable_artist_entity_commision_artist_entity_id` FOREIGN KEY (`artist_entity_id`) REFERENCES {$this->getTable('artist/artist')} (`entity_id`) ON DELETE CASCADE  ON UPDATE CASCADE        
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;
SQL
);
