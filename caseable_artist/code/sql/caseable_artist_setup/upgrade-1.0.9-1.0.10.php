<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$entity                 = Caseable_Artist_Model_Artist::ENTITY;
$entityTypeId           = $installer->getEntityTypeId($entity);
$tblArtistAttrVarchar   = $this->getTable('artist/artist') . '_varchar';

$defaultPercentValue = 6;
$defaultCurrencyCode = Mage::app()->getBaseCurrencyCode();
$prefix  = 'artist_';
$attributeCode  = 'commision_percent';
$attributeCode2 = 'commision_currency';

$attrPercentId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode);
$attrCurrencyId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode2);

if ($attrPercentId){
    $installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE `attribute_id` = '$attrPercentId';");
    //$installer->removeAttribute($entity, $attributeCode);    
    Mage::getModel('catalog/resource_eav_attribute')->load($attrPercentId)->delete();
}
if ($attrCurrencyId){
    
    $installer->run("DELETE FROM `$tblArtistAttrVarchar` WHERE `attribute_id` = '$attrCurrencyId';");
    //$installer->removeAttribute($entity, $attributeCode2);
    Mage::getModel('catalog/resource_eav_attribute')->load($attrCurrencyId)->delete();
}

//Delete mock attribute
$attributeCode3 = "commission_currency";
$attrId = (int) Mage::getSingleton('eav/entity_attribute')->getIdByCode($entity, $attributeCode3);
if ($attrId){
    //$installer->removeAttribute($entity, $attributeCode);
     Mage::getModel('catalog/resource_eav_attribute')->load($attrId)->delete();
}

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, $prefix . $attributeCode, array(
    'type' => 'varchar',
    'label' => 'Commision percent',
    'input' => 'text',
    'required' => true,
    'sort_order' => 300,
    'global' =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group' => 'Billing Information',
    'default' => $defaultPercentValue
));

$installer->addAttribute(Caseable_Artist_Model_Artist::ENTITY, $prefix . $attributeCode2, array(
    'type' => 'varchar',
    'input' => 'select',
    'source' => 'artist/system_config_source_currency',
    'backend' => 'eav/entity_attribute_backend_array',
    'label' => 'Commission currency',
    'required' => true,
    'sort_order' => 310,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'group' => 'Billing Information',
    'default' => $defaultCurrencyCode,
));

$installer->endSetup();
