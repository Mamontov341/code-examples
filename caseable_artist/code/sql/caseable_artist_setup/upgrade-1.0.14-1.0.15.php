<?php

$installer = $this;
$installer->startSetup();

$attributeCode  = 'commision_percent';
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->removeAttribute('artist',$attributeCode);

$installer->endSetup();