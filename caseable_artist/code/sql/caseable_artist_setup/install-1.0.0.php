<?php

/**
 * Caseable artist setup file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */

/* @var $installer Caseable_Artist_Model_Setup */
$installer = $this;
$installer->startSetup();
$installer->getConnection()->resetDdlCache();

########################################################################################################################
#                                               setup artist tables
########################################################################################################################
$table = $installer->getConnection()
    ->newTable($installer->getTable('artist/artist'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Entity ID')
    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => '0',
    ), 'Entity Type ID')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Update Time')
    ->addColumn('sku_pattern', Varien_Db_Ddl_Table::TYPE_CHAR, 3, array(), 'SKU Pattern for Artist')
    ->addColumn('artist_active', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array('unsigned' => true, 'default' => 0,), 'Is Active')
    ->addIndex($installer->getIdxName('artist/artist', array('entity_type_id')),
        array('entity_type_id'))
    ->addForeignKey($installer->getFkName('artist/artist', 'entity_type_id', 'eav/entity_type', 'entity_type_id'),
        'entity_type_id', $installer->getTable('eav/entity_type'), 'entity_type_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Caseable Artist Entity Table');
$installer->getConnection()->createTable($table);

$installer->createStoreBasedEntityTables(
    $this->getTable('artist/artist'),
    array(
        'no-main' => true,
        'no-default-types' => true,
        'types' => array(
            'int' => array(Varien_Db_Ddl_Table::TYPE_INTEGER, null),
            'varchar' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255'),
            'text' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '64k'),
            'decimal'   => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4'),
        )
    )
);

$installer->installEntities();

$installer->endSetup();