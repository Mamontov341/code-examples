<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()->resetDdlCache();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'interview_banner', array(
    'type' => 'varchar',
    'label' => 'Main Banner',
    'input' => 'dropzone_banner',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 100,
    'global' => SCOPE_STORE,
    'group' => 'Interview',
));
$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'interview_intro', array(
    'type' => 'text',
    'label' => 'Intro Text',
    'input' => 'textarea',
    'sort_order' => 200,
    'global' => SCOPE_STORE,
    'group' => 'Interview',
    'required' => false,
    'wysiwyg' => true,
    'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
));
$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'interview_qas', array(
    'type' => 'text',
    'label' => 'Questions and Answers',
    'input' => 'textarea',
    'sort_order' => 300,
    'global' => SCOPE_STORE,
    'group' => 'Interview',
    'required' => false,
    'wysiwyg' => true,
    'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
));
$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'interview_images', array(
    'type' => 'varchar',
    'label' => 'Content Images',
    'input' => 'dropzone_banner',
    'backend' => 'artist/artist_attribute_backend_image',
    'frontend_class' => 'dropzone_upload width-200 height-200',
    'required' => false,
    'sort_order' => 400,
    'global' => SCOPE_STORE,
    'group' => 'Interview',
));

$installer->endSetup();