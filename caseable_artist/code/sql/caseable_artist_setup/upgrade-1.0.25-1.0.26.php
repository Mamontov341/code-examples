<?php
/**
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 06.04.2016
 * Time: 12:33
 */

$installer = $this;

$installer->startSetup();

/*** Update artist_active  attribute*/
$installer->updateAttribute('artist', 'artist_active', 'source_model', "caseable_core/entity_attribute_source_boolean");

$installer->getConnection()->resetDdlCache();
$installer->endSetup();