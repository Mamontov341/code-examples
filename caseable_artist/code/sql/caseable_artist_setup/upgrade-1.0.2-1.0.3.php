<?php

/**
 * Caseable artist sql upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    David Bittner <david.bittner@caseable.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   1.0.3
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn(
    $installer->getTable('artist/artist'),
    'artist_active',
    array(
        'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned' => true,
        'default'  => 2,
        'comment'  => 'Is Active'
    )
);

$installer->updateAttribute(
    Caseable_Artist_Model_Artist::ENTITY,
    'artist_active',
    'source',
    'caseable_core/entity_attribute_source_boolean'
);

$installer->endSetup();