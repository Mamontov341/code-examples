<?php
/**
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 21.12.2015
 * Time: 13:06
 */

/* @var $installer Caseable_Artist_Model_Setup */
$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'frontend_position', array(
    'type' => 'int',
    'auto_increment' => true,
    'unsigned' => true,
    'nullable' => false,
    'label' => 'Frontend Position',
    'input' => 'text',
    'global' => Caseable_Artist_Model_Setup::SCOPE_GLOBAL,
    'sort_order' => 100,
));

$installer->getConnection()->resetDdlCache();
$installer->endSetup();
