<?php

/**
 * rawproduct setup file
 *
 * @category  Caseable
 * @package   Caseable_Rawproduct
 * @author    David Bittner <david.bittner@caseable.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */

/* @var $installer Caseable_Rawproduct_Model_Setup */
$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$installer->getConnection()->addColumn(
    $installer->getTable('artist/artist'),
    'tax_exempt',
    array(
        'type'     => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned' => true,
        'default'  => 2,
        'comment'  => 'Is Tax Exempt'
    )
);

$setup->addAttribute(Caseable_Artist_Model_Artist::ENTITY, 'tax_exempt', array(
    'type' => 'static',
    'label' => 'Tax Exempt',
    'input' => 'select',
    'default' => 2,
    'sort_order' => 900,
    'global' => Caseable_Artist_Model_Setup::SCOPE_GLOBAL,
    'group' => 'Billing Information',
    'source' => 'caseable_core/entity_attribute_source_boolean',
));

$installer->getConnection()->resetDdlCache();
$installer->endSetup();

