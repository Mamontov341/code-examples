<?php

/**
 * artist setup file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */

/* @var $installer Caseable_Artist_Model_Artist */
$installer = $this;
$installer->startSetup();


$field = array("is_wysiwyg_enabled" => "1");

$installer->updateAttribute(Caseable_Artist_Model_Artist::ENTITY, 'profile_text', $field);

$installer->updateAttribute(Caseable_Artist_Model_Artist::ENTITY, 'meta_description', $field);


$installer->getConnection()->resetDdlCache();
$installer->endSetup();
