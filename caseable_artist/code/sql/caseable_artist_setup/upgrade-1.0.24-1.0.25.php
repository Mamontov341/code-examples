<?php
/**
 * Created by PhpStorm.
 * User: Alexey Mamontov
 * Date: 04.01.2016
 * Time: 12:33
 */

$installer = $this;

$installer->startSetup();

$setup = Mage::getModel('eav/entity_setup','core_setup');

$table = $installer->getConnection()
    ->newTable($installer->getTable('caseable_catalog_product'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Entity ID')
    ->addColumn('artist_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Artist ID')
    ->addColumn('draft_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Draft ID')
    ->addColumn('rawproduct_type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Rawproduct Type ID')
    ->addColumn('rawproduct_size_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Rawproduct Size ID')
    ->addColumn('device_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Device ID')
    ->setComment('Caseable Catalog Product Table');
$installer->getConnection()->createTable($table);

$installer->getConnection()->resetDdlCache();
$installer->endSetup();