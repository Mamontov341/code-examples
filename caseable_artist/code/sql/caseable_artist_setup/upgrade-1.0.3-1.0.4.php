<?php

/**
 * Caseable artist sql upgrade file
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    David Bittner <david.bittner@caseable.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   1.0.3
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->updateAttribute(
    Caseable_Artist_Model_Artist::ENTITY,
    'artist_active',
    'source_model',
    'caseable_core/entity_attribute_source_boolean'
);

$installer->endSetup();