<?php

/**
 * Caseable artist adminhtml grid
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */
class Caseable_Artist_Block_Adminhtml_ArtistMenu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Grid constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('artistPositionsGrid');
        $this->setDefaultSort('menu_position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * Get store
     *
     * @return Mage_Core_Model_Store
     */
    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * Prepare the list and set some nice filter for our collection
     * we extend everything here
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('artist/artist')->getCollection();
        $collection->addAttributeToSelect('sku_pattern');
        $collection->addAttributeToSelect('artist_active');
        $collection->addAttributeToSelect('profile_name');
        $collection->addAttributeToSelect('frontend_position');
        $collection->addAttributeToFilter('menu_visible', 1);

        $store = $this->_getStore();

        if ($store->getId() > 0) {
            $collection->joinAttribute('custom_name', Caseable_Artist_Model_Artist::ENTITY . '/profile_name', 'entity_id', null, 'left', $this->_getStore()->getId());
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getMainButtonsHtml()
    {
        $html = '';
        if($this->getFilterVisibility()){
            $html.= $this->getSearchButtonHtml();
        }
        return $html;
    }

    /**
     * Prepare columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header_css_class'  => 'checkbox_p_header',
            'column_css_class' => 'checkbox_p',
            'index' => 'entity_id',
            'type' => 'checkbox',
            'filter' => false,
        ));

        $this->addColumn('image', array(
            'header' => $this->__('Thumbnail'),
            'width' => '1',
            'index' => 'image',
            'escape' => true,
            'sortable' => false,
            'filter' => false,
            'renderer' => 'Caseable_Core_Block_Adminhtml_Entity_Grid_Renderer_Image',
        ));

        $this->addColumn('entity_id', array(
            'header' => $this->__('ID'),
            'width' => '25px',
            'index' => 'entity_id',
            'filter' => false,
        ));

        $this->addColumn('sku_pattern', array(
            'header' => $this->__('SKU'),
            'width' => '25px',
            'index' => 'sku_pattern',
            'filter' => false,
        ));

        $this->addColumn('profile_name', array(
            'header' => $this->__('Artist Name'),
            'width' => '125px',
            'index' => 'profile_name',
            'filter' => false,
        ));

        $store = $this->_getStore();
        if ($store->getId() > 0) {
            $this->addColumn('custom_name', array(
                'header' => $this->__('Name in (%s -> %s)', $store->getGroup()->getName(), $store->getName()),
                'width' => '125px',
                'index' => 'custom_name',
            ));
        }

        $this->addColumn('artist_active', array(
            'header' => $this->__('Status'),
            'width' => '25px',
            'index' => 'artist_active',
            'type' => 'options',
            'options' => Mage::getSingleton('caseable_core/entity_attribute_source_boolean')->getOptionArray(),
        ));


        $this->addColumn('menu_position', array(
            'header' => $this->__('Position in Menu'),
            'header_css_class'  => 'front_p_header',
            'index' => 'menu_position',
            'width' => '30px',
            'type' => 'input',
            'column_css_class' => 'front_p',
            'editable'  => false,
        ));

        return parent::_prepareColumns();
    }

    /**
     * get grid url for ajax filtering
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridPositions', array('_current' => true));
    }

}