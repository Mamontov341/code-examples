<?php

/**
 * Caseable artist adminhtml grid
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Block_Adminhtml_Artist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Grid constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('artistGrid');
        $this->setDefaultSort('frontend_position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * Get store
     *
     * @return Mage_Core_Model_Store
     */
    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * Prepare the list and set some nice filter for our collection
     * we extend everything here
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('artist/artist')->getCollection();
        $collection->addAttributeToSelect('sku_pattern');
        $collection->addAttributeToSelect('artist_active');
        $collection->addAttributeToSelect('profile_name');
        $collection->addAttributeToSelect('frontend_position');

        $store = $this->_getStore();

        if ($store->getId() > 0) {
            $collection->joinAttribute('custom_name', Caseable_Artist_Model_Artist::ENTITY . '/profile_name', 'entity_id', null, 'left', $this->_getStore()->getId());
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('image',
            array(
                'header' => $this->__('Thumbnail'),
                'width' => '1',
                'index' => 'image',
                'escape' => true,
                'sortable' => false,
                'filter' => false,
                'renderer' => 'Caseable_Core_Block_Adminhtml_Entity_Grid_Renderer_Image',
            )
        );
        $this->addColumn('entity_id', array(
            'header' => $this->__('ID'),
            'width' => '25px',
            'index' => 'entity_id'
        ));

        $this->addColumn('sku_pattern', array(
            'header' => $this->__('SKU'),
            'width' => '25px',
            'index' => 'sku_pattern'
        ));

        $this->addColumn('profile_name', array(
            'header' => $this->__('Artist Name'),
            'width' => '200px',
            'index' => 'profile_name'
        ));

        $store = $this->_getStore();
        if ($store->getId() > 0) {
            $this->addColumn('custom_name', array(
                'header' => $this->__('Name in (%s -> %s)', $store->getGroup()->getName(), $store->getName()),
                'width' => '125px',
                'index' => 'custom_name',
            ));
        }

        $this->addColumn('artist_active', array(
            'header' => $this->__('Status'),
            'width' => '25px',
            'index' => 'artist_active',
            'type' => 'options',
            'options' => Mage::getSingleton('caseable_core/entity_attribute_source_boolean')->getOptionArray(),
        ));

        $this->addColumn('updated_at', array(
        'header' => $this->__('Last Change'),
        'index' => 'updated_at',
        'width' => '100px',
        'type' => 'datetime',
        ));

        $this->addColumn('frontend_position', array(
            'header' => $this->__('Frontend Position'),
            'header_css_class'  => 'front_p_header',
            'index' => 'frontend_position',
            'width' => '30px',
            'type' => 'input',
            'column_css_class' => 'front_p',
            'editable'  => false,
        ));

        if (true === Mage::getSingleton('admin/session')->isAllowed('caseable/artists/manage_artists/edit')) {
            $this->addColumn('action', array(
                'header' => $this->__('Action'),
                'width' => '20px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $this->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'is_system' => true,
                'sortable' => false,
            ));
        } else {
            $this->addColumn('action', array(
                'header' => $this->__('Action'),
                'width' => '20px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $this->__('View'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'is_system' => true,
                'sortable' => false,
            ));
        }

        return parent::_prepareColumns();
    }

    /**
     * Prepare mass action
     *
     * @return $this|Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('artist');
        $this->setNoFilterMassactionColumn(true);

        $this->getMassactionBlock()->addItem('delete', array(
            'label'   => 'Delete',
            'url'     => $this->getUrl('*/*/massDelete'),
            'confirm' => 'Are you sure?'
        ));

        $statuses = array('Active', 'Inactive');
        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'      => Mage::helper('catalog')->__('Change status'),
            'url'        => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name'   => 'status',
                    'type'   => 'select',
                    'class'  => 'required-entry',
                    'label'  => Mage::helper('catalog')->__('Status'),
                    'values' => $statuses
                )
            )
        ));

        return $this;
    }

    /**
     * Get grid url for ajax filtering
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * Get row url
     *
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        $store = $this->_getStore();
        if ($store->getId() > 0) {
            return $this->getUrl('*/*/edit', array('id' => $row->getId(), 'store' => $store->getId()));
        } else {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        }
    }
}