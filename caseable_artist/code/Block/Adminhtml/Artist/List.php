<?php

/**
 * Caseable artist adminhtml list
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Block_Adminhtml_Artist_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * List constructor
     */
    public function __construct()
    {
        // XML node for blocks in config.xml
        $this->_blockGroup = 'caseable_artist';

        // Current controller name
        $this->_controller = 'Adminhtml_Artist';

        // Set some headlines
        $this->_headerText = Mage::helper('caseable_artist')->__('Manage Artists');
        $this->_addButtonLabel = Mage::helper('caseable_artist')->__('Create new artist');

        parent::__construct();
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->_addButton('save_pos', array(
            'label'   => Mage::helper('catalog')->__('Save Positions'),
            'onclick' => "savePositions();",
            'class'   => 'add'
        ));

        if (false === Mage::getSingleton('admin/session')->isAllowed('caseable/artists/manage_artists/edit')) {
            $this->_removeButton('add');
        }

        return parent::_prepareLayout();
    }
}