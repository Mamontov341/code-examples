<?php

/**
 * Dropzone file uploads for artist images
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.de). All rights served.
 * @version   0.0.0.1
 */
class Caseable_Artist_Block_Adminhtml_Artist_Edit_Helper_Dropzone_Image extends Varien_Data_Form_Element_Abstract
{
    /**
     * Get profile image url
     *
     * @return string
     */
    protected function _getProfileImageUrl()
    {
        $artist = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
        $path = $artist->getUrlKey() . DS .  'image.jpg';
        return Mage::getSingleton('artist/artist_config')->getMediaUrl($path);
    }

    /**
     * Get profile image url
     *
     * @return string
     */
    protected function _getProfileBannerUrl()
    {
        $artist = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
        $path = $artist->getUrlKey() . DS . 'banner.jpg';
        return Mage::getSingleton('artist/artist_config')->getMediaUrl($path);
    }

    /**
     * Get profile image url
     *
     * @return string
     */
    protected function _getProfileImagePath()
    {
        $artist = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
        $path = $artist->getUrlKey() . DS .  'image.jpg';
        return Mage::getSingleton('artist/artist_config')->getMediaPath($path);
    }

    /**
     * Get profile image url
     *
     * @return string
     */
    protected function _getProfileBannerPath()
    {
        $artist = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
        $path = $artist->getUrlKey() . DS . 'banner.jpg';
        return Mage::getSingleton('artist/artist_config')->getMediaPath($path);
    }

    /**
     * Get dropzone html element
     *
     * @return string
     */
    public function getElementHtml()
    {
        $this->setType('hidden');
        $this->setExtType('hiddenfield');
        $html = parent::getElementHtml();

        if ($this->getHtmlId() == 'profile_image') {
            $url = $this->_getProfileImageUrl();
            $path = $this->_getProfileImagePath();
        } else if ($this->getHtmlId() == 'profile_banner') {
            $url = $this->_getProfileBannerUrl();
            $path = $this->_getProfileBannerPath();
        }

        if (file_exists($path)) {
            $html .= '
                <input type="file" name="file" id="dropzone_' . $this->getHtmlId() . '_file" style="display:none">
                <div class="upload-url" style="display:none">' . $this->getUploadUrl() . '</div>
                <div class="dropzone" id="dropzone_' . $this->getHtmlId() . '" style="display:none">
                    <div class="dropzone-text">
                        <p>Drop files here to upload</p>
                        <p><a>Browse</a></p>
                    </div>
                </div>
                <div class="dropzone-preview">
                    <div class="dropzone-image">
                        <img src="' . $url . '" />
                    </div>
                    <p><a class="dropzone-delete">Delete</a></p>
                </div>
            ';
        } else {
            $html .= '
                <input type="file" name="file" id="dropzone_' . $this->getHtmlId() . '_file" style="display:none">
                <div class="upload-url" style="display:none">' . $this->getUploadUrl() . '</div>
                <div class="dropzone" id="dropzone_' . $this->getHtmlId() . '">
                    <div class="dropzone-text">
                        <p>Drop files here to upload</p>
                        <p><a>Browse</a></p>
                    </div>
                </div>
                <div class="dropzone-preview" style="display:none">
                    <div class="dropzone-image">
                        <img src="" />
                    </div>
                    <p><a class="dropzone-delete">Delete</a></p>
                </div>
            ';
        }

        return $html;
    }

    /**
     * Get image upload url
     *
     * @return string
     */
    public function getUploadUrl()
    {
        return Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/upload/image') . '&isAjax=true';
    }
}