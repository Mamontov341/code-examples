<?php

class Caseable_Artist_Block_Adminhtml_Artist_Edit_Tab_Commision extends Mage_Adminhtml_Block_Widget_Grid {
    /**
     * @var Caseable_Artist_Model_Artist
     */
    protected $_currentEntity = null;
    
    /**
     * Get artist entity
     *
     * @return Caseable_Artist_Model_Artist
     */
    public function getArtistEntity()
    {
        return $this->_currentEntity;
    }
    
    public function __construct() {
        parent::__construct();
        $this->_currentEntity = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
        $this->setId('artist_commision_grid');

        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        parent::_prepareCollection();
        $resource = Mage::getSingleton('core/resource');
        $collection = Mage::getResourceModel('caseable_core/channel_collection');
        
        $collection->getSelect()->joinLeft(
                        array('tblCommision' => $resource->getTableName('artist/commision')), "tblCommision.channel_id = main_table.channel_id AND tblCommision.artist_entity_id = {$this->getArtistEntity()->getId()}", 
                        array('commision'=>'commision'));

        $collection->getSelect()->order('main_table.type', "ASC");
        $collection->getSelect()->order('main_table.name', "ASC");

        $this->setCollection($collection);
        
        return $this;
    }

    protected function _prepareColumns() {
        $helper = Mage::helper('caseable_artist');
        $this->addColumn('name', array(
            'header' => $helper->__('Channel'),
            'index'  => 'name',
            'filter'    => false,
            'sortable'  => false,
        ));        
        
        $this->addColumn('type', array(
            'header'    => $helper->__('Channel type'),
             'index'    => 'type',
            'filter'    => false,
            'sortable'  => false,
        ));
        
        $this->addColumn('commision[]', array(
            'header'    => $helper->__('Commision'),
            'index'     => 'commision',
            'type'      =>'input',
            'inline_css'=>  'qty',
            'validate_class' => 'validate-number',
            'filter'    => false,
            'sortable'  => false,
        ));
        
        $this->addColumn('channel_id[]', array(
            'header'    => $helper->__('Hide'),
            'index'     => 'channel_id',
            'type'      => 'input',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display',
        ));
        
        return parent::_prepareColumns();
    }
}
