<?php

/**
 * Caseable artist adminhtml edit tab attributes
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Block_Adminhtml_Artist_Edit_Tab_Attributes extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @var Caseable_Artist_Model_Artist
     */
    protected $_currentEntity = null;

    /**
     * @var string (form key for entity object)
     */
    protected $formKey = 'artist';

    /**
     * Attributes constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_currentEntity = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
    }

    /**
     * Load Wysiwyg on demand and prepare layout
     *
     * @return Mage_Core_Block_Abstract|void
     */
    protected function _prepareLayout()
    {
        Varien_Data_Form::setElementRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_element'));
        Varien_Data_Form::setFieldsetRenderer($this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset'));
        Varien_Data_Form::setFieldsetElementRenderer($this->getLayout()->createBlock('adminhtml/catalog_form_renderer_fieldset_element'));

        if (!$this->_currentEntity->getId()) {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
    }

    /**
     * Prepare attributes form
     *
     * @return null
     */
    protected function _prepareForm()
    {
        $group = $this->getGroup();
        if ($group) {
            $form = new Varien_Data_Form();

            // Initialize product object as form property to use it during elements generation
            $form->setDataObject($this->_currentEntity);

            // Add fieldset to from
            $fieldset = $form->addFieldset('group_fields' . $group->getId(), array(
                'legend' => $this->__($group->getAttributeGroupName()),
                'class' => 'fieldset-wide'
            ));

            // Get all attributes for this group
            $attributes = $this->getGroupAttributes();

            // Add fieldsets
            $this->_setFieldset($attributes, $fieldset);

            // Get all values
            $values = $this->_currentEntity->getData();

            // Set default attribute values for new type
            if (true === is_null($this->_currentEntity->getId())) {
                foreach ($attributes as $attribute) {
                    if (!isset($values[$attribute->getAttributeCode()])) {
                        $values[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                    }
                }
            }

            // Lock global attributes when store id is not 0
            if ((int)$this->getRequest()->getParam('store', 0) > 0) {
                foreach ($attributes as $attribute) {
                    if (true === $attribute->isScopeGlobal()) {
                        $this->_currentEntity->lockAttribute($attribute->getAttributeCode());
                    }

                }
            }

            // Lock attributes which are scope website if current scope is not default store for website
            if (false === $this->_isDefaultWebsiteStore()) {
                foreach ($attributes as $attribute) {
                    if (true === $attribute->isScopeWebsite()) {
                        $this->_currentEntity->lockAttribute($attribute->getAttributeCode());
                        $fieldset->addField(
                            $attribute->getAttributeCode() . 'note',
                            'note',
                            array('note' => 'This Attribute is website based. Change to default store for selected website to edit this value.'),
                            $attribute->getAttributeCode()
                        );
                    }
                }
            }

            // Lock all attributes if user has only view permissions
            if (false === Mage::getSingleton('admin/session')->isAllowed('caseable/artists/manage_artists/edit')) {
                foreach ($attributes as $attribute) {
                    $this->_currentEntity->lockAttribute($attribute->getAttributeCode());
                }
            }

            // Check for locked attributes
            if ($this->_currentEntity->hasLockedAttributes()) {
                foreach ($this->_currentEntity->getLockedAttributes() as $attribute) {
                    $element = $form->getElement($attribute);
                    if ($element) {
                        $element->setReadonly(true, true);
                    }
                }
            }

            $form->addValues($values);
            $form->setFieldNameSuffix($this->formKey);
            $this->setForm($form);
        }
    }

    /**
     * Retrieve additional element types
     *
     * @return array
     */
    protected function _getAdditionalElementTypes()
    {
        $result = array(
            'dropzone_image' => Mage::getConfig()->getBlockClassName('caseable_artist/adminhtml_artist_edit_helper_dropzone_image'),
            'dropzone_banner' => Mage::getConfig()->getBlockClassName('caseable_artist/adminhtml_artist_edit_helper_dropzone_image'),
            'boolean' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg')
        );

        return $result;
    }

    /**
     * Check if default website store
     *
     * @return bool
     */
    protected function _isDefaultWebsiteStore()
    {
        if ($this->getRequest()->getParam('store', 0) > 0) {
            $defaultStore = Mage::getModel('core/store')->load($this->getRequest()->getParam('store', 0))->getWebsite()->getDefaultStore()->getId();
            if ((int)$this->getRequest()->getParam('store', 0) != $defaultStore) {
                return false;
            }
        }

        return true;
    }
}
