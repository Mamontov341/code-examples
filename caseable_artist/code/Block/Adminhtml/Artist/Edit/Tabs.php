<?php

/**
 * Caseable artist adminhtml edit tab configuration
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Block_Adminhtml_Artist_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * @var Caseable_Artist_Model_Artist
     */
    protected $_currentEntity = null;

    /**
     * @var string (form key for entity object)
     */
    protected $formKey = 'artist';

    /**
     * Tabs constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('artist_info_tabs');
        $this->setDestElementId('artist_edit_form');
        $this->setTitle($this->__('Artist'));
    }

    /**
     * Internal constructor, that is called from real constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_currentEntity = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $setId = $this->_currentEntity->getAttributeSetId();

        if ((int)$setId > 0) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($setId)
                ->setSortOrder()
                ->load();

            foreach ($groupCollection as $group) {
                $attributes = $this->_currentEntity->getAttributes($group->getId(), true);

                // Do not add groups without attributes
                foreach ($attributes as $key => $attribute) {
                    if (1 != (int)$attribute->getIsVisible()) {
                        unset($attributes[$key]);
                    }
                    if($attribute->getAttributeCode() == "frontend_position" || $attribute->getAttributeCode() == "menu_position") {
                        unset($attributes[$key]);
                    }
                }

                if (count($attributes) == 0) {
                    continue;
                }

                // Add tab if tab has visible attributes
                $this->addTab('group_' . $group->getId(), array(
                    'label' => $this->__($group->getAttributeGroupName()),
                    'content' => $this->getLayout()->createBlock('caseable_artist/adminhtml_artist_edit_tab_attributes',
                            'adminhtml.caseable.artist.edit.tab.attributes')->setGroup($group)
                            ->setGroupAttributes($attributes)
                            ->toHtml(),
                ));

            }

            $this->addTab('group_commision' , array(
                    'label' => $this->__('Commision'),
                    'url'   => $this->getUrl('*/*/commision', array('_current' => true)),
                            'class' => 'ajax',
            ));
            
        }

        return parent::_prepareLayout();
    }
}