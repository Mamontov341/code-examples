<?php

/**
 * Caseable artist adminhtml edit form
 *
 * @category  Caseable
 * @package   Caseable_Artist
 * @author    Alexey Mamontov <mamontov341@gmail.com>
 * @copyright 2014 Caseable (http://www.caseable.com)
 * @version   1.0.0
 */
class Caseable_Artist_Block_Adminhtml_Artist_Edit extends Mage_Adminhtml_Block_Widget
{
    /**
     * @var Caseable_Artist_Model_Artist
     */
    protected $_currentEntity = null;

    /**
     * Edit constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('artist/edit.phtml');
        $this->setId('artist_edit');
    }

    /**
     * Internal constructor, that is called from real constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_currentEntity = Mage::registry(Caseable_Artist_Model_Artist::REGISTRY_KEY);
    }

    /**
     * Preparing artist edit layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->setChild('back_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => $this->__('Back'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('*/*/', array('store' => $this->getRequest()->getParam('store', 0))) . '\')',
                    'class' => 'back'
                ))
        );

        if (true === Mage::getSingleton('admin/session')->isAllowed('caseable/artists/manage_artists/delete')) {
            $this->setChild('delete_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label' => $this->__('Delete'),
                        'onclick' => 'confirmSetLocation(\'' . $this->__('Are you sure?') . '\', \'' . $this->getUrl('*/*/delete', array('_current' => true)) . '\')',
                        'class' => 'delete'
                    ))
            );
        }

        if (true === Mage::getSingleton('admin/session')->isAllowed('caseable/artists/manage_artists/edit')) {
            $this->setChild('reset_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label'     =>$this->__('Reset'),
                        'onclick'   => 'window.location.reload()'
                    ))
            );

            $this->setChild('save_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label' => $this->__('Save'),
                        'onclick' => 'artistForm.submit()',
                        'class' => 'save'
                    ))
            );

            $this->setChild('save_and_edit_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setData(array(
                        'label' => $this->__('Save and Continue Edit'),
                        'onclick' => 'artistForm.submit(\'' . $this->getSaveAndContinueUrl() . '\');',
                        'class' => 'save',
                    ))
            );
        }

        parent::_prepareLayout();
    }

    /**
     * Get artist entity
     *
     * @return Caseable_Artist_Model_Artist
     */
    public function getArtistEntity()
    {
        return $this->_currentEntity;
    }

    /**
     * Get save and continue url
     *
     * @return string
     */
    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current' => true,
            'back' => 'edit',
            'active_tab' => null
        ));
    }

    /**
     * Get save url
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current' => true, 'back' => null));
    }

    /**
     * Get delete button html
     *
     * @return string
     */
    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

    /**
     * Get back button html
     *
     * @return string
     */
    public function getBackButtonHtml()
    {
        return $this->getChildHtml('back_button');
    }

    /**
     * Get save button html
     *
     * @return string
     */
    public function getSaveButtonHtml()
    {
        return $this->getChildHtml('save_button');
    }

    /**
     * Get save and edit button html
     *
     * @return string
     */
    public function getSaveAndEditButtonHtml()
    {
        return $this->getChildHtml('save_and_edit_button');
    }

    /**
     * Get reset button html
     *
     * @return string
     */
    public function getResetButtonHtml()
    {
        return $this->getChildHtml('reset_button');
    }

    /**
     * Get edit header text
     *
     * @return string
     */
    public function getHeader()
    {
        if ($this->_currentEntity->getId()) {
            return $this->escapeHtml($this->__('Edit artist ( ' . $this->_currentEntity->getLastName() . ' ' . $this->_currentEntity->getPrefix() . '  )'));
        } else {
            return $this->__('Create new artist');
        }
    }

    /**
     * Get url to validate action inside controller
     *
     * @return string
     */
    public function getValidateUrl()
    {
        return $this->getUrl('*/*/validate', array('_current' => true));
    }
}